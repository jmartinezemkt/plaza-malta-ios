//
//  EventosTableViewController.swift
//  PlazaMalta
//
//  Created by alex on 9/4/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit

import NVActivityIndicatorView
import SwiftyJSON
import Alamofire


class EventosTableViewController: UITableViewController,NVActivityIndicatorViewable {
    
    @IBOutlet weak var open: UIBarButtonItem!
    @IBOutlet var table: UITableView!
    var arrRes = [[String:AnyObject]]() //Array of dictionary
    var text1 = "", text2 = "", text3 = "",  text4 = ""
    var titleevent = "", image = "", descript = "", dateevent = ""

    var imgURL: NSURL!
    override func viewDidLoad() {
        super.viewDidLoad()
          ParseShop()
        GeneralAttributes()
        table.allowsSelection = true

        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrRes.count > 0 {
            return arrRes.count
            
        } else {
            if Connectivity.isConnectedToInternet() {
                
                TableViewHelper.EmptyMessage2(message: "Cargando", viewController: self, table: tableView)
            }else{
                TableViewHelper.EmptyMessage2(message: "Lo sentimos, no tienes conexión a internet, vuelve a intentarlo", viewController: self, table: tableView)
                self.stopAnimating()
            }
            
            TableViewHelper.EmptyMessage2(message: "No hay eventos por el momento", viewController: self, table: tableView)
            self.stopAnimating()
            
            return 0
        }
    }
    override func tableView(_ tableView:UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celdaidentificador = "eventcell"
        for element in arrRes {
            
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: celdaidentificador, for: indexPath) as?
            EventosTableViewCell else{
                fatalError("La instancia de la celda no funciona")
        }
        
        var dict = arrRes[(indexPath as NSIndexPath).row]
        
        cell.title.text = dict["titulo"] as? String
        
        cell.date.text = dict["fecha"] as? String
        
        imgURL = NSURL(string: dict["imagen"] as! String )
        
        if imgURL != nil {
            let data = NSData(contentsOf: (imgURL as URL?)!)
            cell.eventimage.image = UIImage(data: data! as Data)
        }
       
        
        
        return cell
    }
    
    
    override  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        print("You selected cell #\(indexPath.row)!")
        var dict = arrRes[(indexPath as NSIndexPath).row]
        // Get Cell Label
        let indexPath = table.indexPathForSelectedRow;
        let currentCell = table.cellForRow(at: indexPath!) as! EventosTableViewCell!;
        
        text1 =  (currentCell?.title.text)!
        
        image =  imgURL.absoluteString!
        text2 = (dict["fecha"] as? String)!
        text4 = (dict["descripcion"] as? String)!

        
        performSegue(withIdentifier: "event", sender: self)
        
    }

  
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if (segue.identifier == "event") {
        let popOverVC = segue.destination as! detaileventViewController
         popOverVC.titleevent = text1
           popOverVC.datetext = text2
        popOverVC.imageurl = image
            popOverVC.descriptext = text4
        }
    }


    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    func GeneralAttributes(){
        let searchImage = UIImage(named:"logoh.png")?.withRenderingMode(.alwaysOriginal)
        
        
        
        let searchButton = UIBarButtonItem(image: searchImage,  style: .plain, target: self, action: nil)
        
        
        navigationItem.leftBarButtonItems = [self.open, searchButton]

        if self.revealViewController() != nil {
            self.open.target = self.revealViewController()
            self.open.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
        let color1 = self.hexStringToUIColor(hex: "#25333F")
        let color2 = self.hexStringToUIColor(hex: "#2DBEBB")
        
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackOpaque
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationController?.navigationBar.barTintColor = color1;
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: color2]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as! [String : Any]
        let color3 = self.hexStringToUIColor(hex:"435465")
        table.backgroundColor = color3

    }
    
    //Parse our Endpoint
    func ParseShop(){
        startAnimating(type: NVActivityIndicatorType.ballGridBeat)
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.request(Router.getEvents()).responseJSON{ (responseData) -> Void in
            if((responseData.result.value) != nil) {
                
                let swiftvar = JSON(responseData.result.value!)
                
                
                if let jsonvar = swiftvar.arrayObject{
                    self.arrRes = jsonvar as! [[String:AnyObject]]
                    
                    
                    print(responseData.result.error)
                    
                    
                    
                }
                if self.arrRes.count > 0{
                    self.table.reloadData()
                    self.stopAnimating()
                }
                
            }
        }
    }

    
    
}
