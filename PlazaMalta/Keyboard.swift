//
//  Keyboard.swift
//  PlazaMalta
//
//  Created by alex on 9/27/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit

class Keyboard: UIViewController,UITextFieldDelegate  {

    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    weak var activeField: UITextField?
    
    var animateContenetView = true
    var originalContentOffset: CGPoint?
    var isKeyboardVisible = false
    
    let offset : CGFloat = 18
    
    
    //MARK: - IBAction
    
    @IBAction func AnimateSwitchValueChanged(sender: UISwitch) {
        animateContenetView = sender.isOn
    }
    
    
    //MARK: - life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for case let textField as UITextField in contentView.subviews {
            textField.delegate = self
        }
        
        let notificationCenter = NotificationCenter.defaultCenter
        notificationCenter.addObserver(self, selector: #selector(ViewController.keyboardWillBeShown(_:)), name: UIKeyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(ViewController.keyboardWillBeHidden(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.activeField = nil
    }
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.activeField = textField
    }
    
    
    func keyboardWillBeShown(notification: NSNotification) {
        originalContentOffset = scrollView.contentOffset
        
        if let activeField = self.activeField, let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            var visibleRect = self.scrollView.bounds
            visibleRect.size.height -= keyboardSize.size.height
            
            //that's to avoid enlarging contentSize multiple times in case of many UITextFields,
            //when user changes an edited text field
            if isKeyboardVisible == false {
                scrollView.contentSize.height += keyboardSize.height
            }
            
            //scroll only if the keyboard would cover a bottom edge of an
            //active field (including the given offset)
            let activeFieldBottomY = activeField.frame.origin.y + activeField.frame.size.height + offset
            let activeFieldBottomPoint = CGPoint(x: activeField.frame.origin.x, y: activeFieldBottomY)
            if (!visibleRect.contains(activeFieldBottomPoint)) {
                var scrollToPointY = activeFieldBottomY - (self.scrollView.bounds.height - keyboardSize.size.height)
                scrollToPointY = min(scrollToPointY, scrollView.contentSize.height - scrollView.frame.size.height)
                
                scrollView.setContentOffset(CGPoint(x: 0, y: scrollToPointY), animated: animateContenetView)
            }
        }
        
        isKeyboardVisible = true
    }
    
    
    func keyboardWillBeHidden(notification: NSNotification) {
        scrollView.contentSize.height = contentView.frame.size.height
        if var contentOffset = originalContentOffset {
            contentOffset.y = min(contentOffset.y, scrollView.contentSize.height - scrollView.frame.size.height)
            scrollView.setContentOffset(contentOffset, animated: animateContenetView)
        }
        
        isKeyboardVisible = false
    }
    
    
     func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: nil) { (_) -> Void in
            self.scrollView.contentSize.height = self.contentView.frame.height
        }
    }
    
    
    deinit {
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}
