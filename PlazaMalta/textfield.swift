//
//  textfield.swift
//  PlazaMalta
//
//  Created by elaniin on 10/6/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit

class textfield: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 5);
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }

}
