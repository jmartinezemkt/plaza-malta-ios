//
//  Wifi.swift
//  PlazaMalta
//
//  Created by alex on 8/28/17.
//  Copyright © 2017 elaniin. All rights reserved.
//
import NVActivityIndicatorView
import SwiftyJSON
import Alamofire



class Wifi: ViewController,NVActivityIndicatorViewable {
    
   
    var arrRes = [[String:AnyObject]]() //Array of dictionary

    
    @IBOutlet weak var open: UIBarButtonItem!
   
    @IBOutlet weak var password: UILabel!
    @IBOutlet weak var name: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundPlayer = BackgroundVideo(on: self, withVideoURL: "login.mp4") // Passing self and video name with extension
        backgroundPlayer?.pause()
        startAnimating(type: NVActivityIndicatorType.ballGridBeat)
        Alamofire.request("https://toolboxsv.com/dev/plaza_malta/wp-json/v1/general").responseJSON{ (responseData) -> Void in
            if((responseData.result.value) != nil) {
                self.stopAnimating()
                if let JSON = responseData.result.value as? [String: String]{
                    print(JSON["wifi-name"])
                    self.name.text = JSON["wifi-name"]
                    self.password.text = JSON["wifi-pass"]
                    
                 
                    
                }
                }
                
            }
        
        if self.revealViewController() != nil {
            open.target = self.revealViewController()
            open.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        let color1 = self.hexStringToUIColor(hex: "#25333F")
        let color2 = self.hexStringToUIColor(hex: "#2DBEBB")
        
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        
        self.navigationController?.navigationBar.barTintColor = color1;
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: color2]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as! [String : Any]
        
        
        
        }
        
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    

    

    


}
