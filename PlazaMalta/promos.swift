//
//  promos.swift
//  PlazaMalta
//
//  Created by alex on 9/13/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import NVActivityIndicatorView
import SwiftyJSON
import Alamofire
import TBEmptyDataSet
import Reachability


class promos:  UIViewController,NVActivityIndicatorViewable, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate  {

    @IBOutlet weak var open: UIBarButtonItem!
    @IBOutlet weak var view2: UIView!
 
   
    @IBOutlet var viewh: UICollectionView!
    var imgURL: NSURL!, imgURL2: NSURL!
    
    var arrRes = [[String:AnyObject]]() //Array of dictionar
    var data1="", data2="",data3=""
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if self.revealViewController() != nil {
            open.target = self.revealViewController()
            open.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
        self.viewh.emptyDataSetDataSource = self
        self.viewh.emptyDataSetDelegate = self
       
        General()
     
        let reachability = Reachability()!
        
        switch reachability.connection {
        case .wifi:
             ParseShop()
        case .cellular:
             ParseShop()
        case .none:
            var alert = UIAlertView(title: "Sin conexión", message: "Debes tener una conexión a Internet", delegate: nil, cancelButtonTitle: "OK")
            self.stopAnimating()
            alert.show()
        }
    }
    
    
   
    func General(){
 
        let searchImage = UIImage(named:"logoh.png")?.withRenderingMode(.alwaysOriginal)

        let searchButton = UIBarButtonItem(image: searchImage,  style: .plain, target: self, action: nil)
        

        navigationItem.leftBarButtonItems = [self.open, searchButton]

        let color1 = self.hexStringToUIColor(hex: "#25333F")
        
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackOpaque
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        let color3 = self.hexStringToUIColor(hex: "#2DBEBB")
        self.navigationController?.navigationBar.barTintColor = color1;
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: color3]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : Any]
        self.title = "Promociones"
        
        viewh!.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
        let cellWidth = ((UIScreen.main.bounds.width) - 36 - 54 ) / 2
        if let layout = self.viewh.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 15, left: 20, bottom: 15, right: 20)
        
    }

     func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    
    //Populate view
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "promocell", for: indexPath) as! CellCollectionViewCell
        
        
        cell.contentView.layer.cornerRadius = 10
        cell.contentView.layer.borderWidth = 1.0
        
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true
        
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
       
            
        
        
        
        var dict = arrRes[(indexPath as NSIndexPath).row]
        imgURL = NSURL(string: dict["imagen"] as! String )
        
        
        cell.title.text = dict["titulo"] as! String
  
        
        if imgURL != nil {
            let data = NSData(contentsOf: (imgURL as URL?)!)
            let image = UIImage(data: data! as Data)
        
            cell.image.image = image
            cell.image.clipsToBounds = true
            
            if (image?.size.width)! > (image?.size.height)! {
                cell.image.contentMode = UIViewContentMode.scaleAspectFit
                
                //since the width > height we may fit it and we'll have bands on top/bottom
                cell.image.widthAnchor.constraint(equalTo: (cell.image.heightAnchor), multiplier: (image?.size.width)! / (image?.size.height)!).isActive = true
            } else {
                cell.image.contentMode = UIViewContentMode.scaleAspectFill
                //width < height we fill it until width is taken up and clipped on top/bottom
                cell.image.widthAnchor.constraint(equalTo: (cell.image.heightAnchor), multiplier: (image?.size.width)! / (image?.size.height)!).isActive = true
            }
        }
        else{
            let image = UIImage(named: "store3")!
            cell.image.image = image
            let cellWidth = ((UIScreen.main.bounds.width) - 36 - 54 ) / 2
            let sizeOfImage = image.size
            
           
        }
        
         return cell
    }
    
    
 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrRes.count

        }
    
    

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var reusableview: UICollectionReusableView? = nil
        if kind == UICollectionElementKindSectionFooter {
            reusableview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "FooterView", for: indexPath)
            if arrRes.count > 0 {
                reusableview?.isHidden = true
                reusableview?.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            }
            else {
                reusableview?.isHidden = false
                reusableview?.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
            }
        }
        return reusableview ?? UICollectionReusableView()
    }

    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "promosegue" {
            var secondScene = segue.destination as! detailpromoViewController
            
            let cell = sender as! CellCollectionViewCell
            let indexPath = self.viewh.indexPath(for: cell)
            var dict = arrRes[(indexPath! as NSIndexPath).row]
            imgURL2 = NSURL(string: dict["imagen"] as! String )
            secondScene.titlepromo = dict["titulo"] as! String
            secondScene.imagelogo = try! imgURL2.absoluteString!
            
            secondScene.descrippromo = dict["descripcion"] as! String

        }
    }
 

    

        //Parse our Endpoint
        func ParseShop(){
            self.startAnimating(type: NVActivityIndicatorType.ballGridBeat)
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            Alamofire.request(Router.getPromos()).responseJSON{ (responseData) -> Void in
                if((responseData.result.value) != nil) {
                    
                    
                    let swiftvar = JSON(responseData.result.value!)
                    
                    
                    if let jsonvar = swiftvar.arrayObject{
                        self.arrRes = jsonvar as! [[String:AnyObject]]
                      self.stopAnimating()
                        
                        print(responseData.result.error ?? "")
                        
                        
                    }
                    if self.arrRes.count > 0{
                        
                        self.viewh.reloadData()
                        self.stopAnimating()
                        
                       
                    }else{
                        self.stopAnimating()
                    }
                    
                }
            }
        

    }

   

    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    


 
}



extension promos : PinterestLayoutDelegate
{
    func collectionView(collectionView: UICollectionView, heightForPhotoAt indexPath: IndexPath, with width: CGFloat) -> CGFloat
    {
        let cell = self.viewh.cellForItem(at: indexPath)  as? CellCollectionViewCell
            var dict = arrRes[(indexPath as NSIndexPath).row]
        imgURL = NSURL(string: dict["imagen"] as! String )
            if imgURL != nil {
                
                let data = NSData(contentsOf: (imgURL as URL?)!)
                let image = UIImage(data: data! as Data)
                cell?.image.clipsToBounds = true
                cell?.image.widthAnchor.constraint(equalTo: (cell?.image.heightAnchor)!, multiplier: (image?.size.width)! / (image?.size.height)!).isActive = true
                
                let sizeOfImage = image?.size
                let boundingRect = CGRect(x: 0, y: 0, width: width, height: CGFloat(MAXFLOAT))
                let rect = AVMakeRect(aspectRatio: sizeOfImage!, insideRect: boundingRect)
                return rect.size.height
            
            
        }
        
        
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, heightForCaptionAt indexPath: IndexPath, with width: CGFloat) -> CGFloat
    {
        let cell = self.viewh.cellForItem(at: indexPath)  as? CellCollectionViewCell
        var dict = arrRes[(indexPath as NSIndexPath).row]
        let p = dict["titulo"]?.description
        if p != nil {
            cell?.title.text = p
            let topPadding = CGFloat(8)
            let bottomPadding = CGFloat(12)
            let captionFont = UIFont.systemFont(ofSize: 16)
            let captionHeight = self.height(for: p!, with: captionFont, width: width)
            let profileImageHeight = CGFloat(17)
            let height = topPadding + captionHeight + topPadding + profileImageHeight + bottomPadding
            return height
            
            
        }
        
     
        return 0.0
    }
    
    func height(for text: String, with font: UIFont, width: CGFloat) -> CGFloat
    {
        
        let nsstring = NSString(string: text)
        let maxHeight = CGFloat(64.0)
        let textAttributes = [NSFontAttributeName : font]
        let boundingRect = nsstring.boundingRect(with: CGSize(width: width, height: maxHeight), options: .usesLineFragmentOrigin, attributes: textAttributes, context: nil)
      return ceil(boundingRect.height)
        return 100
    }



}













