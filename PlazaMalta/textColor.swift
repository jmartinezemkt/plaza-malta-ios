//
//  textColor.swift
//  PlazaMalta
//
//  Created by alex on 9/5/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import Foundation

class textColor: UIDatePicker {
    var changed = false
    override func addSubview(_ view: UIView) {
        if !changed {
            changed = true
            self.setValue(UIColor.white, forKey: "textColor")
        }
        super.addSubview(view)
    }
}
