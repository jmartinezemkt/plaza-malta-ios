//
//  DetailViewController.swift
//  PlazaMalta
//
//  Created by alex on 8/31/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SwiftyJSON
import Alamofire
import MessageUI


class DetailViewController: UIViewController,NVActivityIndicatorViewable,MFMailComposeViewControllerDelegate{

    @IBOutlet weak var emailshop: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var logosshop: UIImageView!
    
    @IBOutlet weak var background: UIView!
    var arrRes = [[String:AnyObject]]() //Array 
    @IBOutlet weak var tel2: UILabel!
    //@IBOutlet weak var openfood: UIButton!
    
    @IBOutlet weak var openfood: UIButton!
    @IBOutlet weak var backimage: UIView!
    @IBOutlet weak var telshop1: UILabel!
    
    @IBOutlet weak var facebookb: UIButton!
    
    @IBOutlet weak var instab: UIButton!
    @IBOutlet weak var twb: UIButton!
    
    var  titleshop = String()
    var desciption:String = String()
    var logo:String = String()
    var website:String = String()
    var logoshop:String = String()
    var email:String = String()
    var facebook:String = String()
    var twitter:String = String()
    var instagram:String = String()
    var tel:String = String()
    var telshop2:String = String()
    var getfood = String()
    var op1="",op2="",op3="",op4="",op5="",op6="",op7=""
    var cp1="",cp2="",cp3="",cp4="",cp5="",cp6="",cp7=""
    
    @IBOutlet weak var hora1: UILabel!
    @IBOutlet weak var hora2: UILabel!
    @IBOutlet weak var hora3: UILabel!
    @IBOutlet weak var hora4: UILabel!
    @IBOutlet weak var hora5: UILabel!
    @IBOutlet weak var hora6: UILabel!
    @IBOutlet weak var hora7: UILabel!
    @IBOutlet weak var tellabel: UILabel!
    @IBOutlet weak var labelemail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
            general()
       self.title = ""
        //Show data
            
        self.name.text = "ABIERTO HASTA LAS "+titleshop
        
            self.telshop1.text = tel
            self.tel2.isHidden = true
            self.hora1.text = op1+"-"+cp1
            self.hora2.text = op2+"-"+cp2
            self.hora3.text = op3+"-"+cp3
            self.hora4.text = op4+"-"+cp4
            self.hora5.text = op5+"-"+cp6
            self.hora6.text = op6+"-"+cp7
            self.hora7.text = op7+"-"+cp7
        
            if(self.email == ""){
                self.labelemail.isHidden = true
                self.emailshop.isHidden = true
            }else{
                self.emailshop.text = email
            }
        
        
            if(tel2 != nil){
                self.tel2.isHidden = false
                self.tel2.text = telshop2
            }
            if((tel.isEmpty) && (self.telshop2.isEmpty)){
                self.tellabel.isHidden = true
            }
        
        self.navigationController?.navigationBar.backItem?.title = ""
    
        loadImageFromUrl(url: logo, view: logosshop)
      
            if(self.facebook == ""){
                 facebookb.isHidden = true
            }else if(self.twitter == ""){
                 twb.isHidden = true
            }else if(self.instagram == ""){
                 instab.isHidden = true
            }
        
        if(self.getfood == ""){
                self.openfood.isHidden = true
            }
        
    
          print(getfood)
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closePopUp(_ sender: AnyObject) {
        self.removeAnimate()
        //self.view.removeFromSuperview()
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
     func loadImageFromUrl(url: String, view: UIImageView){
        
        // Create Url from string
        let url = NSURL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = URLSession.shared.dataTask(with: url as URL) { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                DispatchQueue.main.async(execute: { () -> Void in
                    let image = UIImage(data: data)
                    
                    view.image = image
                 
              
                })
            }
        }
        
        // Run task
        task.resume()
    }


    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    
    @IBAction func fb(_ sender: Any) {
        
        if(self.facebook != ""){
        var fbUrl = NSURL(string: facebook)
        if UIApplication.shared.canOpenURL(fbUrl! as URL)
        {
            UIApplication.shared.openURL(fbUrl! as URL)
            
        } else {
            //redirect to safari because the user doesn't have Instagram
            UIApplication.shared.openURL(NSURL(string: facebook)! as URL)
        }
        }else{
            facebookb.isHidden = true
        }
        
    }
    
    @IBAction func Instagram(_ sender: Any) {
        
        if(self.instagram != ""){
            var instaUrl = NSURL(string: instagram)
            if UIApplication.shared.canOpenURL(instaUrl! as URL)
            {
                UIApplication.shared.openURL(instaUrl! as URL)
                
            } else {
                //redirect to safari because the user doesn't have Instagram
                UIApplication.shared.openURL(NSURL(string: instagram)! as URL)
            }
        }else{
            instab.isHidden = true
            
        }
        
        
        
    }
    
    
    @IBAction func twitter(_ sender: Any) {
        
        if(self.twitter != ""){
            var twUrl = NSURL(string: twitter)
            if UIApplication.shared.canOpenURL(twUrl! as URL)
            {
                UIApplication.shared.openURL(twUrl! as URL)
                
            } else {
                //redirect to safari because the user doesn't have Instagram
                UIApplication.shared.openURL(NSURL(string: twitter)! as URL)
            }
        }else{
            twb.isHidden = true
        }
    

    }
    
    func general(){
        self.view.frame = UIScreen.main.bounds
        self.view.isOpaque = false
        self.view.backgroundColor = .clear
        backimage.backgroundColor = UIColor.white.withAlphaComponent(1)
        logosshop.backgroundColor = UIColor.white.withAlphaComponent(1)
        telshop1.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(gestureRecognizer:)))
        
        telshop1.addGestureRecognizer(tap)
        
        tel2.isUserInteractionEnabled = true
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(handleTap2(gestureRecognizer:)))
        
        tel2.addGestureRecognizer(tap2)
        
        emailshop.isUserInteractionEnabled = true
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(handleTap3(gestureRecognizer:)))
        
        emailshop.addGestureRecognizer(tap3)
        
        
        
        
    }
    
    @IBAction func goBack(_ sender: AnyObject) {
     
        DispatchQueue.main.async(execute: {
            self.dismiss(animated: true) {
                print("finished")
            }})
        
    }

    func handleTap(gestureRecognizer: UIGestureRecognizer) {
        if let url = URL(string: "tel://\(tel)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func handleTap2(gestureRecognizer: UIGestureRecognizer) {
        if let url = URL(string: "tel://\(telshop2)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func handleTap3(gestureRecognizer: UIGestureRecognizer) {
        let url = NSURL(string: "mailto:"+email)
        UIApplication.shared.openURL(url as! URL)
    }
    
    @IBAction func openFoodmenu(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var popOverVC = storyboard.instantiateViewController(withIdentifier: "webview") as! Webviewmenus
        
        popOverVC.getLink = self.getfood
        self.present(popOverVC, animated: true , completion: nil)
    }
    
    
    
}


