//
//  detaileventViewController.swift
//  PlazaMalta
//
//  Created by alex on 9/18/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit

class detaileventViewController: UIViewController {
    var titleevent = String()
    var imageurl = String()
    var datetext = String()
    var descriptext = String()
    @IBOutlet weak var eventitle: UILabel!
    
    @IBOutlet weak var dateevent: UILabel!
    @IBOutlet weak var navigationitem: UINavigationItem!
   
    @IBOutlet weak var descripevent: UITextView!
 
    
    @IBOutlet weak var nav: UINavigationBar!

    @IBOutlet weak var images: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        eventitle.text = titleevent
        descripevent.text = descriptext
        dateevent.text = datetext
        
        loadImageFromUrl(url: imageurl, view: images)
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadImageFromUrl(url: String, view: UIImageView){
        
        // Create Url from string
        let url = NSURL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = URLSession.shared.dataTask(with: url as URL) { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                DispatchQueue.main.async(execute: { () -> Void in
                    view.image = UIImage(data: data)
                })
            }
        }
        
        // Run task
        task.resume()
    }

    
    @IBAction func goBack(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
        
    }
}
