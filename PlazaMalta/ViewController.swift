import UIKit
import AVKit
import AVFoundation

var backgroundPlayer : BackgroundVideo?
class ViewController: UIViewController {
    var player: AVPlayer?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backgroundPlayer = BackgroundVideo(on: self, withVideoURL: "login.mp4") // Passing self and video name with extension
        backgroundPlayer?.setUpBackground()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    
}

