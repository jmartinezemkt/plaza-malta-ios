//
//  Registro2ViewController.swift
//  PlazaMalta
//
//  Created by alex on 9/5/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit

class Registro2ViewController: UIViewController,UITextFieldDelegate{

    @IBOutlet weak var email: UITextField!
    
    //---Variables//
    var name = String()
    var birthday = String()
    var getErrorMessages = errorMessages()
    //------//
    
    override func viewDidLoad() {
        super.viewDidLoad()

        /* Textfield like a singlesline*/
        GeneralAttributes.borderBottonmTextField(textField: email)
        email.delegate = self
        /*-------------------------*/
        
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        view.addGestureRecognizer(tap)
        //-----------//
        
    }
    
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewControllerB = segue.destination as? Registro3ViewController {
            if(self.email.text=="" ){

               
                GeneralAttributes.showAlertMessage(titleStr: getErrorMessages.titleError, messageStr: getErrorMessages.emaiFieldlEmpty, fromController: self)
            }
                
            else {
                
                if GeneralAttributes.isValidEmail(testStr: self.email.text!){
                   
                    viewControllerB.name = self.name
                    viewControllerB.email = self.email.text!
                    viewControllerB.birthday = self.birthday
                }
                else{
                   GeneralAttributes.showAlertMessage(titleStr: getErrorMessages.titleError, messageStr: getErrorMessages.invalidEmail, fromController: self)
                    
                }

               
            }
        }
         if let viewControlleA = segue.destination as? RegistroViewController {
            viewControlleA.nombrereturn = self.name
            viewControlleA.dateborn = self.birthday
        
        }
    }
    
    //Calls this function when the tap is recognized.
     func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    

}
