//
//  Registro3ViewController.swift
//  PlazaMalta
//
//  Created by alex on 9/5/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import PasswordTextField



class Registro3ViewController: UIViewController {
    
    /*Decalaration of IBOutlets*/
    @IBOutlet weak var confirm: UITextField!
    @IBOutlet weak var password: UITextField!
    /*------------*/
    
    /*---variables---*/
    var email = String()
    var name = String()
    var birthday = String()
    var getErrorMessages = errorMessages()
    var ref: DatabaseReference!
    /*------------*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        
        /* Textfield like a singlesline*/
        GeneralAttributes.borderBottonmTextField(textField: password)
        
        
        ///*Confirm line at the bottom */
        
        GeneralAttributes.borderBottonmTextField(textField: confirm)
        
        
        /*-------------------------*/
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(Registro3ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func signup(_ sender: Any) {
            }
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        
        if password.text=="" || (self.password.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
           
            GeneralAttributes.showAlertMessage(titleStr:getErrorMessages.titleError, messageStr: getErrorMessages.passwordFieldEmpty, fromController: self)
            
            return false
        }else if confirm.text == "" {
            
        
            GeneralAttributes.showAlertMessage(titleStr: getErrorMessages.titleError, messageStr: getErrorMessages.confirmFieldEmpty, fromController: self)
            return false
        }
        else
        {
            if  GeneralAttributes.isPasswordValid(password.text!){
                if self.confirm.text == self.password.text{
                    
                    Auth.auth().createUser(withEmail: email, password: password.text!, completion: { (user, error) in
                        if let error = error {
                            if let errCode = AuthErrorCode(rawValue: error._code) {
                                switch errCode {
                                case .invalidEmail :
                                    self.showAlert("Su email es invalido")
                                case .emailAlreadyInUse:
                                    self.showAlert("Su email ya esta resgitrado")
                                default:
                                    self.showAlert("Error: \(error.localizedDescription)")
                                }
                            }
                            return
                        }
                        self.signIn()
                    })

                }else{
                    GeneralAttributes.showAlertMessage(titleStr: getErrorMessages.titleError, messageStr: getErrorMessages.incompatiblePasswords, fromController: self)
                }
                
            }
            else{
                GeneralAttributes.showAlertMessage(titleStr: getErrorMessages.titleError, messageStr: getErrorMessages.lengthPasswordError, fromController: self)
                
            }

            return true;
        }
        
     
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    

    @IBAction func didTapBackToLogin(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {})
    }
    
    func showAlert(_ message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Cerrar", style: UIAlertActionStyle.default,handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func signIn() {
        if GeneralAttributes.isPasswordValid(password.text!){
        self.ref.child("users").child((Auth.auth().currentUser?.uid)!).setValue(["username": name,"email": email, "Nombre": name, "birthday": birthday, "photo": "null"])
            
        performSegue(withIdentifier: "SignUp", sender: nil)
        }else{
            GeneralAttributes.showAlertMessage(titleStr: getErrorMessages.titleError, messageStr: getErrorMessages.siginEmailError, fromController: self)
        }
}
    
    
}
