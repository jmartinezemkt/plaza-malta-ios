//
//  RegistroViewController.swift
//  PlazaMalta
//
//  Created by alex on 9/4/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit


class RegistroViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate,UITextFieldDelegate
{
    
    var pickOption = [["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"
        ,"21","22","23","24","25","26","27","28","29","30","31"], ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]]
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return pickOption.count
    }
    
    var nombrereturn = String(), dateborn = String()
    
    
    @IBOutlet weak var datetext: UITextField!
 
    @IBOutlet weak var name: UITextField!
  
    let pickerView = UIPickerView()
    let picker = UIDatePicker()
    
    @IBOutlet weak var sepick: UIPickerView!
    
    @IBOutlet weak var progress: UIProgressView!
    
    
    let sizeComponent = 0
    let toppingComponent = 1
    var getErrorMessages = errorMessages()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(nombrereturn != nil){
            self.name.text = nombrereturn
        }
     
        GeneralAttributes.borderBottonmTextField(textField: name)
        
        /*-------------------------*/
        pickerView.dataSource = self
        pickerView.delegate = self
        datetext.inputView = pickerView
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
   
        
        view.addGestureRecognizer(tap)
        
        
        
        name.delegate = self
        datetext.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
 
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption[component].count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[component][row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let color = pickOption[0][pickerView.selectedRow(inComponent: 0)]
        let model = pickOption[1][pickerView.selectedRow(inComponent: 1)]
        datetext.text = color + "/" + model
    }
    
  
 
    
    
   
    
    
    @IBAction func next(_ sender: Any) {
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewControllerA = segue.destination as? Registro2ViewController {
            if(self.name.text!=="" || (self.name.text?.trimmingCharacters(in: .whitespaces).isEmpty)!){
                
                GeneralAttributes.showAlertMessage(titleStr: getErrorMessages.titleError, messageStr: getErrorMessages.errorName, fromController: self)
                
            }else{
                
                viewControllerA.name = self.name.text!
                
                viewControllerA.birthday = self.datetext.text!
                
            }
        }
        
        
        
        
    }
    
    //Calls this function when the tap is recognized.
     func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    
    
    
    
    
    
}
