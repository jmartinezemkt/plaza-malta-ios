//
//  LoginViewController.swift
//  PlazaMalta
//
//  Created by alex on 8/30/17.
//  Copyright © 2017 elaniin. All rights reserved.
//
import GoogleSignIn
import UIKit
import Firebase

import NVActivityIndicatorView
import FBSDKLoginKit


class LoginViewController: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate, NVActivityIndicatorViewable,UITextFieldDelegate{

    @IBOutlet weak var password: UITextField!
  
    @IBOutlet weak var plaza: UITextView!
    @IBOutlet weak var back: UIView!

    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var loginbutton: UIButton!
    @IBOutlet weak var email: UITextField!
    var offsetY:CGFloat = 0
    var getErrorMessages = errorMessages()
  private var fbLoginSuccess = false //This is gobal
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginbutton.sizeToFit()
        loginbutton.sizeToFit()
        email.delegate = self
        password.delegate = self
        backgroundPlayer = BackgroundVideo(on: self, withVideoURL: "login.mp4") // Passing self and video name with extension
        backgroundPlayer?.setUpBackground()
        self.back.backgroundColor = .clear
        
        self.plaza.layer.borderColor = UIColor.white.cgColor
        self.plaza.layer.borderWidth = 0
        self.plaza.layer.backgroundColor = UIColor.clear.cgColor
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self

        email.layer.cornerRadius = 5.0
        password.layer.cornerRadius = 5.0
        loginbutton.layer.cornerRadius = 5.0
        
  
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))

        view.addGestureRecognizer(tap)

    }
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardFrameChangeNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
    }
    func keyboardFrameChangeNotification(notification: Notification) {
        if let userInfo = notification.userInfo {
            let endFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect
            let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double ?? 0
            let animationCurveRawValue = (userInfo[UIKeyboardAnimationCurveUserInfoKey] as? Int) ?? Int(UIViewAnimationOptions.curveEaseInOut.rawValue)
            let animationCurve = UIViewAnimationOptions(rawValue: UInt(animationCurveRawValue))
            if let _ = endFrame, endFrame!.intersects(self.password.frame) {
                self.offsetY = self.password.frame.maxY - endFrame!.minY
                UIView.animate(withDuration: animationDuration, delay: TimeInterval(0), options: animationCurve, animations: {
                    self.password.frame.origin.y = self.password.frame.origin.y - self.offsetY
                }, completion: nil)
            } else {
                if self.offsetY != 0 {
                    UIView.animate(withDuration: animationDuration, delay: TimeInterval(0), options: animationCurve, animations: {
                        self.password.frame.origin.y = self.password.frame.origin.y + self.offsetY
                        self.offsetY = 0
                    }, completion: nil)
                }
            }
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
      
    }
    
    
    
    
    //Calls this function when the tap is recognized.
     func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
        
    }
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    // at this, we use the email as the auth method
    @IBAction func signin(_ sender: Any) {
        startAnimating(type: NVActivityIndicatorType.ballGridBeat)
        Auth.auth().signIn(withEmail: email.text!, password: password.text!) { (user, error) in
            if let error = error {
                self.stopAnimating() 
                if(self.email.text=="" || (self.email.text?.trimmingCharacters(in: .whitespaces).isEmpty)!){
                    GeneralAttributes.showAlertMessage(titleStr: self.getErrorMessages.titleError, messageStr: self.getErrorMessages.emaiFieldlEmpty, fromController: self)
                }else if(self.password.text == ""){
                   
                    GeneralAttributes.showAlertMessage(titleStr: self.getErrorMessages.titleError, messageStr: self.getErrorMessages.passwordSiginInFieldEmpty, fromController: self)
                }
                else{
                    print(error.localizedDescription)
                    GeneralAttributes.showAlertMessage(titleStr: self.getErrorMessages.titleError, messageStr: self.getErrorMessages.invalidSignInData, fromController: self)
                }
                

                return
            }else{
                print("OK")
                 self.performSegue(withIdentifier: "email", sender: self)
                 self.stopAnimating()
                }

                return
            }
        }
    
 
    @IBAction func facebook(_ sender: Any ) {

       
        let facebookLogin = FBSDKLoginManager()
        
        facebookLogin.logIn(withReadPermissions: ["public_profile", "email"], from: self, handler: {
            (facebookResult, facebookError) -> Void in
            if facebookError != nil {
                print("Facebook login failed. Error \(String(describing: facebookError))")
            } else if (facebookResult?.isCancelled)! {
                print("Facebook login was cancelled.")
            } else {
                let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                
                Auth.auth().signIn(with: credential) { (user, error) in
                    if error != nil {
                        print("Login failed. \(String(describing: error))")
                    } else {
                        self.fbLoginSuccess = true
                        print("Logged in!")
                        
                        if (facebookResult?.grantedPermissions.contains("email"))! {
                            
                        }
                        self.performSegue(withIdentifier: "email", sender: self)
                    }
                }
            }
        })
        
        
        
    }
    
    
    
    @IBAction func saveguest(_ sender: Any) {
        
        let defaults = UserDefaults.standard
        defaults.set("invitados", forKey: menuTableViewController.defaultsKeys.keyTwo)
        
    }
    
    
    
    func signIn(credential:AuthCredential){
        Auth.auth().signIn(with: credential) { (user, error) in
            if let error = error {
                print("error:\(error)")
                return
            } else {
                self.performSegue(withIdentifier: "email", sender: nil)
            }
            return
        }
    }
    

    

   
    
    @IBAction func googlesign(_ sender: Any) {
       
      
        GIDSignIn.sharedInstance().signIn()
        
        
    }
    func sign(_ signIn: GIDSignIn!,
              present viewController: menuTableViewController!) {
        DispatchQueue.main.async{
            self.present(viewController, animated: true, completion: nil)
        }
      
    }


    private func sign(_ signIn: GIDSignIn!,
              dismiss viewController: LoginViewController!) {
        DispatchQueue.main.async{
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    


    
    func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
                dismissViewController viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error.localizedDescription)
            return
        }
        
        let authentication = user.authentication
        let credential = GoogleAuthProvider.credential(withIDToken: (authentication?.idToken)!,
                                                       accessToken: (authentication?.accessToken)!)
        Auth.auth().signIn(with: credential) { (user, error) in
            if error == nil  {
                //successfull login
                let databaseRef = Database.database().reference()
                
                databaseRef.child("Users").child(user!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    let snapshot = snapshot.value as? NSDictionary
                    
                    if(snapshot == nil)
                    {
                        
                        
                         databaseRef.child("Users").child((Auth.auth().currentUser?.uid)!).setValue(["username": user?.displayName,"email": user?.email, "Nombre": user?.displayName, "birthday": "null", "photo": "null"])
                    }})
                self.performSegue(withIdentifier: "email", sender: self)
                
              
                }
                
                
            }
        }
    }

