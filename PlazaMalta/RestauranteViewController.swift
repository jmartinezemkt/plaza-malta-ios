//
//  RestauranteViewController.swift
//  PlazaMalta
//
//  Created by alex on 8/24/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit
import XLPagerTabStrip


class RestauranteViewController: ButtonBarPagerTabStripViewController {
   
    @IBOutlet weak var Scroll: UIScrollView!

    
    @IBOutlet weak var open: UIBarButtonItem!
    @IBOutlet weak var nav: UINavigationItem!
    override func viewDidLoad() {

        
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .black
        settings.style.selectedBarBackgroundColor = .white
        
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 3.0
        let color3 = self.hexStringToUIColor(hex: "#2DBEBB")
        settings.style.selectedBarBackgroundColor = color3
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .white
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .white
            let color1 = self?.hexStringToUIColor(hex: "#2DBEBB")
            newCell?.label.textColor = color1
            
            
            
        }
        if self.revealViewController() != nil {
            open.target = self.revealViewController()
            open.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        let color1 = self.hexStringToUIColor(hex: "#25333F")
        let color2 = self.hexStringToUIColor(hex: "#2DBEBB")
        
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        
        self.navigationController?.navigationBar.barTintColor = color1;
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: color2]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as! [String : Any]
        
        super.viewDidLoad()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let rechild_1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "reschild1")
        let reschild_2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "reschild1")
        return [rechild_1, reschild_2]
    }
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
}
