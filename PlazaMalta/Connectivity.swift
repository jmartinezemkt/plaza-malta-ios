//
//  Connectivity.swift
//  PlazaMalta
//
//  Created by elaniin on 10/6/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
