//
//  CargarTiendasViewController.swift
//  PlazaMalta
//
//  Created by elaniin on 10/24/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON
import Alamofire
import Kingfisher
import SwiftDate
import NVActivityIndicatorView
import Reachability

class CargarTiendasViewController: UIViewController,NVActivityIndicatorViewable, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var open: UIBarButtonItem!
    @IBOutlet var tableshop: UITableView!
    let dropDown = DropDown()
    var arrRes = [[String:AnyObject]]() //Array of dictionary
    let choosecategories = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.choosecategories
        ]
    }()
    var savestate = Bool()
    var getAPI = callUrl()
   let color1 = GeneralAttributes.hexStringToUIColor(hex: "#25333F")
    @IBOutlet weak var button1: UIButton!
    var messageList = [Categories]()
    var showEmpyDataSet:Int = -1
    let myNewString:String = ""
    var titleshop:String = "", instagram:String = ""
    var desciption:String = "", logo:String = ""
    var website:String = "", logoshop:String = ""
    var email:String = "",tel:String = "",tel2:String = ""
    var facebook:String = "", twitter:String = "", closeshop:String = ""
    var currently:Date!, dateopen:Date!
    var dateclose:Date!
    let dates = DateInRegion()
    let dateFormatter = DateFormatter()
    let today = NSDate()
    var textimage = "",texttel1 = "" , texttel2 = ""
    var textemail = "", textfb = "", texttw = "" , textinsta = ""
    var imgURL: NSURL!,imgURL2: NSURL!
    let celdaidentificador = "shopcell"
    var selct = Int()
  let color3 = GeneralAttributes.hexStringToUIColor(hex: "#2DBEBB")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.savestate = false
        
    
        let reachability = Reachability()!
        
        switch reachability.connection {
        case .wifi:
            Data3()
        case .cellular:
            Data3()
        case .none:
            var alert = UIAlertView(title: "Sin conexión", message: "Debes tener una conexión a Internet", delegate: nil, cancelButtonTitle: "OK")
            self.stopAnimating()
            alert.show()
        }

        General()
       
        
    }
   
    
    func General(){
       
        let origImage = UIImage(named: "arrow")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        button1.setImage(tintedImage, for: .normal)
        button1.tintColor = .white
        button1.semanticContentAttribute = .forceRightToLeft
        if self.revealViewController() != nil {
            self.open.target = self.revealViewController()
            self.open.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
        let color2 = GeneralAttributes.hexStringToUIColor(hex: "#435465")
        self.tableshop.backgroundColor = color2
        let searchImage = GeneralAttributes.imageItembar(image: "logoh.png")
        
        
        
        let searchButton = UIBarButtonItem(image: searchImage,  style: .plain, target: self, action: nil)
        
        button1.semanticContentAttribute = .forceRightToLeft
        
        navigationItem.leftBarButtonItems = [self.open, searchButton]
        
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackOpaque
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        
        
        self.navigationController?.navigationBar.barTintColor = color1;
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: color3]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as! [String : Any]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func setupChoose1(categories:[Categories]) {
        
        let appearance = DropDown.appearance()
        
        
        appearance.backgroundColor = color1
        appearance.textColor = UIColor.white
        appearance.selectionBackgroundColor = color3
        choosecategories.anchorView = button1
        choosecategories.bottomOffset = CGPoint(x: 0, y: button1.bounds.height)
        choosecategories.dataSource = categories.map { "\($0.namecategories)"
            
        }
        self.choosecategories.selectRow(at: 0)
        
        /*** IMPORTANT PART FOR CUSTOM CELLS ***/
        self.choosecategories.cellNib = UINib(nibName: "MyCell", bundle: nil)
        choosecategories.selectionAction = { [unowned self] (index: Int, item: String) in
            let selectedVehicle = self.messageList[index]
            self.savestate = true
            self.button1.setTitle(item+" ", for: .normal)
            
            
           self.choosecategories.selectRow(at: 0)
            
                self.Data2(getid: selectedVehicle.id.description)
            
            
        }
        
        // And if you use custom cells:
        dropDown.customCellConfiguration = { index, item, cell in
            guard let cell = cell as? MyCell else { return }
            
            // Get your model for this cell the same way as above:
            let vehicle = self.messageList[index]
            
            // Setup your custom UI components
            cell.suffixLabel.text = "\(vehicle.id)"
        }
    }
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrRes.count > 0 {
            return arrRes.count
            self.stopAnimating()
        } else {
            if Connectivity.isConnectedToInternet() {
                self.startAnimating()
                TableViewHelper.EmptyMessage2(message: "Seleccione una categorìa", viewController: self, table: tableView)
            }else{
                TableViewHelper.EmptyMessage2(message: "Lo sentimos, no tienes conexión a internet, vuelve a intentarlo", viewController: self, table: tableView)
                self.stopAnimating()
            }
            
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        for element in arrRes {
            //print(element)
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: celdaidentificador, for: indexPath) as?
            TiendasTableViewCell else{
                fatalError("La instancia de la celda no funciona")
        }
        var dict = arrRes[(indexPath as NSIndexPath).row]
       
        
        cell.button.addTarget(self,action:#selector(buttonAction),
                              for:.touchUpInside)
        let color2 = GeneralAttributes.hexStringToUIColor(hex: "#228c00")
        let color1 = GeneralAttributes.hexStringToUIColor(hex: "#FF2626")
        
       
        
        cell.descriptionshop.scrollRangeToVisible(NSMakeRange(0,0))
        cell.descriptionshop.text = dict["descripcion"] as? String
        cell.button.tag = indexPath.row
        cell.button.addTarget(self,action:#selector(buttonAction),for:.touchUpInside)
        imgURL = NSURL(string: dict["logo"] as! String )
        
        if imgURL != nil {
            
            cell.imagesshop.kf.setImage(with: imgURL! as URL)
        }
        else{
            let image = UIImage(named: "store3")!
            cell.imagesshop.image = image
        }
        
        
        self.dateFormatter.dateFormat = "EEEE"
        self.dateFormatter.locale = Locale(identifier: "en_US")
        let dayOfWeekString = self.dateFormatter.string(from: today as Date)
        
        
        
        switch dayOfWeekString {
            
            /*-------------Show for Monday*/
        case "Monday":
            
            if let Temp = dict["horarios"] as? NSDictionary {
                
                
                if let close = Temp["cierre_lunes"] as? String{
                    
                    self.closeshop = close
                    
                    /* Get current hour from json*/
                    
                    self.dateFormatter.dateFormat = "HH:mm"
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.dateclose = dateFormatter.date(from: close)!
                    
                    /* ------------------------------*/
                    
                    
                }
                
                if let open = Temp["apertura_lunes"] as? String
                    
                {
                    
                    /* Get current hour from json*/
                    
                    self.dateFormatter.dateFormat = "HH:mm"
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.dateopen = dateFormatter.date(from: open)!
                    
                    /* ------------------------------*/
                    
                    
                    /* Get current hour*/
                    
                    var str = dates.string(format: .custom("HH:mm")) // example output:
                    
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.currently = dateFormatter.date(from: str)
                    
                    
                    /* ------------------------------*/
                    
                    
                    
                    // Compare them and show
                    switch self.currently.compare(self.dateopen ) {
                    case .orderedAscending     :
                        if(currently < dateopen || currently > dateclose){
                            let components = Calendar.current.dateComponents([.month,.day,.hour,.minute,.second], from: self.dateopen, to: self.currently)
                            
                            let a = Int(components.hour!)
                            let showhour = (a < 0 ? -a : a);
                            if(showhour > 0){
                                cell.hour.text = "Abre en \(showhour) hrs"
                                
                            }else if(showhour == 0){
                                cell.hour.text = "Abre en 1 hrs."
                                cell.hour.textColor = color1
                                cell.clock.tintColor = color1
                            }
                            
                            
                        }
                        print("Date A is earlier than date B")
                        break
                        
                    case .orderedDescending    :
                        
                        print("",currently,dateopen, dateclose)
                        if(currently >= dateopen && currently <= dateclose){
                            cell.hour.text = "ABIERTO"
                            cell.hour.textColor = color2
                        }else if(currently < dateopen || currently > dateclose){
                            
                            let components = Calendar.current.dateComponents([.minute,.hour], from: self.dateopen, to: self.currently)
                            let a = Int(components.hour!)
                            
                            let str2 = "01:00"
                            dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                            
                            let comparision2 = dateFormatter.date(from: str2)
                            
                            
                            if (self.currently! > dateclose ){
                                cell.hour.text = "Faltan \(a)"
                                
                            }
                                
                            else if(comparision2! == currently && comparision2! < dateopen){
                                var c:Int = -8
                                print(abs(c))
                                
                                let components = Calendar.current.dateComponents([.minute,.hour], from: self.currently, to: self.dateopen)
                                let a = Int(components.hour!)
                                let str = "\(a)"
                                cell.hour.text = "Faltan"
                                
                                
                            }
                            
                        }
                        print("Date A is later than date B")
                        break
                        
                    case .orderedSame         :
                        cell.hour.text = "ABIERTO"
                        cell.hour.textColor = color2
                    default:
                        
                        cell.hour.text = ""
                        
                        break
                        
                    }
                    
                }
                
            }
            
            
            
            break
            
            /*-------------Show for Tuesday*/
        case "Tuesday":
            
            if let Temp = dict["horarios"] as? NSDictionary {
                
                
                if let close = Temp["cierre_martes"] as? String{
                    
                    self.closeshop = close
                    
                    /* Get current hour from json*/
                    
                    self.dateFormatter.dateFormat = "HH:mm"
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.dateclose = dateFormatter.date(from: close)!
                    
                    /* ------------------------------*/
                    
                    
                }
                
                if let open = Temp["apertura_martes"] as? String
                    
                {
                    
                    /* Get current hour from json*/
                    
                    self.dateFormatter.dateFormat = "HH:mm"
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.dateopen = dateFormatter.date(from: open)!
                    
                    /* ------------------------------*/
                    
                    
                    /* Get current hour*/
                    
                    var str = dates.string(format: .custom("HH:mm")) // example output:
                    
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.currently = dateFormatter.date(from: str)
                    
                    
                    /* ------------------------------*/
                    
                    
                    
                    // Compare them and show
                    switch self.currently.compare(self.dateopen ) {
                    case .orderedAscending     :
                        if(currently < dateopen || currently > dateclose){
                            let components = Calendar.current.dateComponents([.month,.day,.hour,.minute,.second], from: self.dateopen, to: self.currently)
                            
                            let a = Int(components.hour!)
                            let showhour = (a < 0 ? -a : a);
                            if(showhour > 0){
                                cell.hour.text = "Abre en \(showhour) hrs."
                                
                            }else if(showhour == 0){
                                cell.hour.text = "Abre en 1 hrs."
                            }
                            
                            
                        }
                        print("Date A is earlier than date B")
                        break
                        
                    case .orderedDescending    :
                        
                        print("",currently,dateopen, dateclose)
                        if(currently >= dateopen && currently <= dateclose){
                            cell.hour.text = "ABIERTO"
                            cell.hour.textColor = color2
                            
                        }else if(currently < dateopen || currently > dateclose){
                            
                            let components = Calendar.current.dateComponents([.minute,.hour], from: self.dateopen, to: self.currently)
                            let a = Int(components.hour!)
                            
                            let str2 = "01:00"
                            dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                            
                            let comparision2 = dateFormatter.date(from: str2)
                            
                            
                            if (self.currently! > dateclose ){
                                cell.hour.text = "Faltan \(a)"
                                
                            }
                                
                            else if(comparision2! == currently && comparision2! < dateopen){
                                var c:Int = -8
                                print(abs(c))
                                
                                let components = Calendar.current.dateComponents([.minute,.hour], from: self.currently, to: self.dateopen)
                                let a = Int(components.hour!)
                                let str = "\(a)"
                                cell.hour.text = "Faltan"
                                
                                
                            }
                            
                        }
                        print("Date A is later than date B")
                        break
                        
                    case .orderedSame         :
                        cell.hour.text = "ABIERTO"
                        cell.hour.textColor = color2
                    default:
                        
                        cell.hour.text = ""
                        
                        break
                        
                    }
                    
                }
                
            }
            
            
            
            
            
            break
            
            /*-------------Show for Wednesday*/
            
        case "Wednesday":
            
            if let Temp = dict["horarios"] as? NSDictionary {
                
                
                if let close = Temp["cierre_miercoles"] as? String{
                    
                    self.closeshop = close
                    
                    /* Get current hour from json*/
                    
                    self.dateFormatter.dateFormat = "HH:mm"
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.dateclose = dateFormatter.date(from: close)!
                    
                    /* ------------------------------*/
                    
                    
                }
                
                if let open = Temp["apertura_miercoles"] as? String
                    
                {
                    
                    /* Get current hour from json*/
                    
                    self.dateFormatter.dateFormat = "HH:mm"
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.dateopen = dateFormatter.date(from: open)!
                    
                    /* ------------------------------*/
                    
                    
                    /* Get current hour*/
                    
                    var str = dates.string(format: .custom("HH:mm")) // example output:
                    
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.currently = dateFormatter.date(from: str)
                    
                    
                    /* ------------------------------*/
                    
                    
                    
                    // Compare them and show
                    switch self.currently.compare(self.dateopen ) {
                    case .orderedAscending     :
                        if(currently < dateopen || currently > dateclose){
                            let components = Calendar.current.dateComponents([.month,.day,.hour,.minute,.second], from: self.dateopen, to: self.currently)
                            
                            let a = Int(components.hour!)
                            let showhour = (a < 0 ? -a : a);
                            if(showhour > 0){
                                cell.hour.text = "Abre en \(showhour) hrs."
                                
                            }else if(showhour == 0){
                                cell.hour.text = "Abre en 1 hrs."
                            }
                            
                            
                        }
                        print("Date A is earlier than date B")
                        break
                        
                    case .orderedDescending    :
                        
                        print("",currently,dateopen, dateclose)
                        if(currently >= dateopen && currently <= dateclose){
                            cell.hour.text = "ABIERTO"
                            cell.hour.textColor = color2
                        }else if(currently < dateopen || currently > dateclose){
                            
                            let components = Calendar.current.dateComponents([.minute,.hour], from: self.dateopen, to: self.currently)
                            let a = Int(components.hour!)
                            
                            let str2 = "01:00"
                            dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                            
                            let comparision2 = dateFormatter.date(from: str2)
                            
                            
                            if (self.currently! > dateclose ){
                                cell.hour.text = "Faltan \(a)"
                                
                            }
                                
                            else if(comparision2! == currently && comparision2! < dateopen){
                                var c:Int = -8
                                print(abs(c))
                                
                                let components = Calendar.current.dateComponents([.minute,.hour], from: self.currently, to: self.dateopen)
                                let a = Int(components.hour!)
                                let str = "\(a)"
                                cell.hour.text = "Faltan"
                                
                                
                            }
                            
                        }
                        print("Date A is later than date B")
                        break
                        
                    case .orderedSame         :
                        cell.hour.text = "ABIERTO"
                        
                    default:
                        
                        cell.hour.text = ""
                        
                        break
                        
                    }
                    
                }
                
            }
            
            
            
            
            break
            
            /*-------------Show for Thursday*/
        case "Thursday":
            if let Temp = dict["horarios"] as? NSDictionary {
                
                
                if let close = Temp["cierre_jueves"] as? String{
                    
                    self.closeshop = close
                    
                    /* Get current hour from json*/
                    
                    self.dateFormatter.dateFormat = "HH:mm"
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.dateclose = dateFormatter.date(from: close)!
                    
                    /* ------------------------------*/
                    
                    
                }
                
                if let open = Temp["apertura_jueves"] as? String
                    
                {
                    
                    /* Get current hour from json*/
                    
                    self.dateFormatter.dateFormat = "HH:mm"
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.dateopen = dateFormatter.date(from: open)!
                    
                    /* ------------------------------*/
                    
                    
                    /* Get current hour*/
                    
                    var str = dates.string(format: .custom("HH:mm")) // example output:
                    
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.currently = dateFormatter.date(from: str)
                    
                    
                    /* ------------------------------*/
                    
                    
                    
                    // Compare them and show
                    switch self.currently.compare(self.dateopen ) {
                    case .orderedAscending     :
                        if(currently < dateopen || currently > dateclose){
                            let components = Calendar.current.dateComponents([.month,.day,.hour,.minute,.second], from: self.dateopen, to: self.currently)
                            
                            let a = Int(components.hour!)
                            let showhour = (a < 0 ? -a : a);
                            if(showhour > 0){
                                cell.hour.text = "Abre en \(showhour) hrs."
                                
                            }else if(showhour == 0){
                                cell.hour.text = "Abre en 1 hrs."
                            }
                            
                            
                        }
                        print("Date A is earlier than date B")
                        break
                        
                    case .orderedDescending    :
                        
                        print("",currently,dateopen, dateclose)
                        if(currently >= dateopen && currently <= dateclose){
                            cell.hour.text = "ABIERTO"
                            cell.hour.textColor = color2
                        }else if(currently < dateopen || currently > dateclose){
                            
                            let components = Calendar.current.dateComponents([.minute,.hour], from: self.dateopen, to: self.currently)
                            let a = Int(components.hour!)
                            
                            let str2 = "01:00"
                            dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                            
                            let comparision2 = dateFormatter.date(from: str2)
                            
                            
                            if (self.currently! > dateclose ){
                                cell.hour.text = "Faltan \(a)"
                                
                            }
                                
                            else if(comparision2! == currently && comparision2! < dateopen){
                                var c:Int = -8
                                print(abs(c))
                                
                                let components = Calendar.current.dateComponents([.minute,.hour], from: self.currently, to: self.dateopen)
                                let a = Int(components.hour!)
                                let str = "\(a)"
                                cell.hour.text = "Faltan"
                                
                                
                            }
                            
                        }
                        print("Date A is later than date B")
                        break
                        
                    case .orderedSame         :
                        cell.hour.text = "ABIERTO"
                        
                    default:
                        
                        cell.hour.text = ""
                        
                        break
                        
                    }
                    
                }
                
            }
            
            
            
            
            break
            
            /*-------------Show for Friday*/
        case "Friday":
            if let Temp = dict["horarios"] as? NSDictionary {
                
                
                if let close = Temp["cierre_viernes"] as? String{
                    
                    self.closeshop = close
                    
                    /* Get current hour from json*/
                    
                    self.dateFormatter.dateFormat = "HH:mm"
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.dateclose = dateFormatter.date(from: close)!
                    
                    /* ------------------------------*/
                    
                    
                }
                
                if let open = Temp["apertura_viernes"] as? String
                    
                {
                    
                    /* Get current hour from json*/
                    
                    self.dateFormatter.dateFormat = "HH:mm"
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.dateopen = dateFormatter.date(from: open)!
                    
                    /* ------------------------------*/
                    
                    
                    /* Get current hour*/
                    
                    var str = dates.string(format: .custom("HH:mm")) // example output:
                    
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.currently = dateFormatter.date(from: str)
                    
                    
                    /* ------------------------------*/
                    
                    
                    
                    // Compare them and show
                    switch self.currently.compare(self.dateopen ) {
                    case .orderedAscending     :
                        if(currently < dateopen || currently > dateclose){
                            let components = Calendar.current.dateComponents([.month,.day,.hour,.minute,.second], from: self.dateopen, to: self.currently)
                            
                            let a = Int(components.hour!)
                            let showhour = (a < 0 ? -a : a);
                            if(showhour > 0){
                                cell.hour.text = "Abre en \(showhour) hrs."
                                
                            }else if(showhour == 0){
                                cell.hour.text = "Abre en 1 hrs."
                            }
                            
                            
                        }
                        print("Date A is earlier than date B")
                        break
                        
                    case .orderedDescending    :
                        
                        print("",currently,dateopen, dateclose)
                        if(currently >= dateopen && currently <= dateclose){
                            cell.hour.text = "ABIERTO"
                            cell.hour.textColor = color2
                        }else if(currently < dateopen || currently > dateclose){
                            
                            let components = Calendar.current.dateComponents([.minute,.hour], from: self.dateopen, to: self.currently)
                            let a = Int(components.hour!)
                            
                            let str2 = "01:00"
                            dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                            
                            let comparision2 = dateFormatter.date(from: str2)
                            
                            
                            if (self.currently! > dateclose ){
                                cell.hour.text = "Faltan \(a)"
                                
                            }
                                
                            else if(comparision2! == currently && comparision2! < dateopen){
                                var c:Int = -8
                                print(abs(c))
                                
                                let components = Calendar.current.dateComponents([.minute,.hour], from: self.currently, to: self.dateopen)
                                let a = Int(components.hour!)
                                let str = "\(a)"
                                cell.hour.text = "Faltan"
                                
                                
                            }
                            
                        }
                        print("Date A is later than date B")
                        break
                        
                    case .orderedSame         :
                        cell.hour.text = "ABIERTO"
                        
                    default:
                        
                        cell.hour.text = ""
                        
                        break
                        
                    }
                    
                }
                
            }
            
            
            
            
            break
            
            
            
            /*-------------Show for Saturday*/
        case "Saturday":
            if let Temp = dict["horarios"] as? NSDictionary {
                
                
                if let close = Temp["cierre_sabado"] as? String{
                    
                    self.closeshop = close
                    
                    /* Get current hour from json*/
                    
                    self.dateFormatter.dateFormat = "HH:mm"
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.dateclose = dateFormatter.date(from: close)!
                    
                    /* ------------------------------*/
                    
                    
                }
                
                if let open = Temp["apertura_sabado"] as? String
                    
                {
                    
                    /* Get current hour from json*/
                    
                    self.dateFormatter.dateFormat = "HH:mm"
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.dateopen = dateFormatter.date(from: open)!
                    
                    /* ------------------------------*/
                    
                    
                    /* Get current hour*/
                    
                    var str = dates.string(format: .custom("HH:mm")) // example output:
                    
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.currently = dateFormatter.date(from: str)
                    
                    
                    /* ------------------------------*/
                    
                    
                    
                    // Compare them and show
                    switch self.currently.compare(self.dateopen ) {
                    case .orderedAscending     :
                        if(currently < dateopen || currently > dateclose){
                            let components = Calendar.current.dateComponents([.month,.day,.hour,.minute,.second], from: self.dateopen, to: self.currently)
                            
                            let a = Int(components.hour!)
                            let showhour = (a < 0 ? -a : a);
                            if(showhour > 0){
                                cell.hour.text = "Abre en \(showhour) hrs."
                                
                            }else if(showhour == 0){
                                cell.hour.text = "Abre en 1 hrs."
                            }
                            
                            
                        }
                        print("Date A is earlier than date B")
                        break
                        
                    case .orderedDescending    :
                        
                        print("",currently,dateopen, dateclose)
                        if(currently >= dateopen && currently <= dateclose){
                            cell.hour.text = "ABIERTO"
                            cell.hour.textColor = color2
                        }else if(currently < dateopen || currently > dateclose){
                            
                            let components = Calendar.current.dateComponents([.minute,.hour], from: self.dateopen, to: self.currently)
                            let a = Int(components.hour!)
                            
                            let str2 = "01:00"
                            dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                            
                            let comparision2 = dateFormatter.date(from: str2)
                            
                            
                            if (self.currently! > dateclose ){
                                cell.hour.text = "Faltan \(a)"
                                
                            }
                                
                            else if(comparision2! == currently && comparision2! < dateopen){
                                var c:Int = -8
                                print(abs(c))
                                
                                let components = Calendar.current.dateComponents([.minute,.hour], from: self.currently, to: self.dateopen)
                                let a = Int(components.hour!)
                                let str = "\(a)"
                                cell.hour.text = "Faltan"
                                
                                
                            }
                            
                        }
                        print("Date A is later than date B")
                        break
                        
                    case .orderedSame         :
                        cell.hour.text = "ABIERTO"
                        
                    default:
                        
                        cell.hour.text = ""
                        
                        break
                        
                    }
                    
                }
                
            }
            
            
            
            
            break
            
            /*-------------Show for Sunday*/
        case "Sunday":
            if let Temp = dict["horarios"] as? NSDictionary  {
                
                
                if let close = Temp["cierre_domingo"] as? String {
                    
                    self.closeshop = close
                    
                    /* Get current hour from json*/
                    
                    self.dateFormatter.dateFormat = "HH:mm"
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    if (close != ""){
                        self.dateclose = dateFormatter.date(from: close)!
                    }else{
                        self.dateclose = dateFormatter.date(from: "19:00")!
                    }
                    /* ------------------------------*/
                    
                    
                }
                
                if let open = Temp["apertura_domingo"] as? String
                    
                {
                    
                    /* Get current hour from json*/
                    
                    self.dateFormatter.dateFormat = "HH:mm"
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    if (open != ""){
                        self.dateopen = dateFormatter.date(from: open)!
                    }else{
                        self.dateopen = dateFormatter.date(from: "9:00")!
                    }
                    /* ------------------------------*/
                    
                    
                    /* Get current hour*/
                    
                    var str = dates.string(format: .custom("HH:mm")) // example output:
                    
                    self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                    self.currently = dateFormatter.date(from: str)
                    
                    
                    /* ------------------------------*/
                    
                    
                    
                    // Compare them and show
                    switch self.currently.compare(self.dateopen ) {
                    case .orderedAscending     :
                        if(currently < dateopen || currently > dateclose){
                            let components = Calendar.current.dateComponents([.month,.day,.hour,.minute,.second], from: self.dateopen, to: self.currently)
                            
                            let a = Int(components.hour!)
                            let showhour = (a < 0 ? -a : a);
                            if(showhour > 0){
                                cell.hour.text = "Abre en \(showhour) hrs."
                                
                                
                            }else if(showhour == 0){
                                cell.hour.text = "Abre en 1 hrs."
                            }
                            
                            
                        }
                        print("Date A is earlier than date B")
                        break
                        
                    case .orderedDescending    :
                        
                        print("",currently,dateopen, dateclose)
                        if(currently >= dateopen && currently <= dateclose){
                            cell.hour.text = "ABIERTO"
                            cell.hour.textColor = color2
                        }else if(currently < dateopen || currently > dateclose){
                            
                            let components = Calendar.current.dateComponents([.minute,.hour], from: self.dateopen, to: self.currently)
                            let a = Int(components.hour!)
                            
                            let str2 = "01:00"
                            dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                            
                            let comparision2 = dateFormatter.date(from: str2)
                            
                            
                            if (self.currently! > dateclose ){
                                cell.hour.text = "Faltan \(a)"
                                
                            }
                                
                            else if(comparision2! == currently && comparision2! < dateopen){
                                var c:Int = -8
                                print(abs(c))
                                
                                let components = Calendar.current.dateComponents([.minute,.hour], from: self.currently, to: self.dateopen)
                                let a = Int(components.hour!)
                                let str = "\(a)"
                                cell.hour.text = "Faltan"
                                
                                
                            }
                            
                        }
                        print("Date A is later than date B")
                        break
                        
                    case .orderedSame         :
                        cell.hour.text = "ABIERTO"
                        
                    default:
                        
                        cell.hour.text = ""
                        
                        break
                        
                    }
                    
                }
                
            }
            
            
            
            
            break
            
            
            
        default: break
            
        }
        if cell.hour.text == "ABIERTO"{
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.clock.image = cell.clock.image!.withRenderingMode(.alwaysTemplate)
        cell.clock.tintColor =  color2
            cell.hour.textColor = color2
        }else{
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.clock.image = cell.clock.image!.withRenderingMode(.alwaysTemplate)
            cell.clock.tintColor =  color1
            cell.hour.textColor = color1
        }
        return cell
    }
    
    
    
    func buttonAction(sender: UIButton) {
        
        let selectedIndex = IndexPath(row: sender.tag, section: 0)
        var dict = arrRes[(selectedIndex as NSIndexPath).row]
        // Get Cell Label
        let indexPath = self.tableshop.indexPathForSelectedRow;
        // And finally do whatever you need using this index :
        
        
        tableshop.selectRow(at: selectedIndex, animated: true, scrollPosition: .none)
        tableshop.selectRow(at: selectedIndex, animated: true, scrollPosition: .none)
        // Now if you need to access the selected cell instead of just the index path, you could easily do so by using the table view's cellForRow method
        
        let selectedCell = tableshop.cellForRow(at: selectedIndex) as! TiendasTableViewCell
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var popOverVC = storyboard.instantiateViewController(withIdentifier: "sbPopUpID2") as! DetailViewController
        let Temp = dict["horarios"]
        if let Temp = dict["horarios"] as? NSDictionary {

            if let close = Temp["cierre_lunes"] as? String{
                popOverVC.cp1 = close.description
            }
            
            if let open = Temp["apertura_lunes"] as? String
                
            {
                popOverVC.op1 = open.description
                
                
            }
            
        }
        if let Temp = dict["horarios"] as? NSDictionary {
            
            if let close = Temp["cierre_martes"] as? String{
                popOverVC.cp2 = close.description
            }
            
            if let open = Temp["apertura_martes"] as? String
                
            {
                popOverVC.op2 = open.description
                
                
            }
            
        }
        
        if let Temp = dict["horarios"] as? NSDictionary {
            
            if let close = Temp["cierre_miercoles"] as? String{
                popOverVC.cp3 = close.description
            }
            
            if let open = Temp["apertura_miercoles"] as? String
                
            {
                popOverVC.op3 = open.description
                
                
            }
            
        }
        if let Temp = dict["horarios"] as? NSDictionary {
            
            if let close = Temp["cierre_jueves"] as? String{
                popOverVC.cp4 = close.description
            }
            
            if let open = Temp["apertura_jueves"] as? String
                
            {
                popOverVC.op4 = open.description
                
                
            }
            
        }
        if let Temp = dict["horarios"] as? NSDictionary {
            
            if let close = Temp["cierre_viernes"] as? String{
                popOverVC.cp5 = close.description
            }
            
            if let open = Temp["apertura_viernes"] as? String
                
            {
                popOverVC.op5 = open.description
                
                
            }
            
        }
        if let Temp = dict["horarios"] as? NSDictionary {
            
            if let close = Temp["cierre_sabado"] as? String{
                popOverVC.cp6 = close.description
            }
            
            if let open = Temp["apertura_sabado"] as? String
                
            {
                popOverVC.op6 = open.description
                
                
            }
            
        }
        
        
        
        if let Temp = dict["horarios"] as? NSDictionary {
            
            
            if let close = Temp["cierre_domingo"] as? String{
               popOverVC.cp7 = close.description
                
                
            }
            
            if let open = Temp["apertura_domingo"] as? String
                
            {
                popOverVC.op7 = open.description
              
                
            }
            
        }
        
        popOverVC.logo = (dict["logo"] as? String)!
        
        popOverVC.email = (dict["email"] as? String)!
        popOverVC.tel = (dict["telefono"] as? String)!
        popOverVC.telshop2 = (dict["telefono2"] as? String)!
        popOverVC.title = (Temp?["cierre_lunes"] as? String)!
        popOverVC.facebook = (dict["facebook"] as? String)!
        popOverVC.twitter = (dict["twitter"] as? String)!
        popOverVC.instagram = (dict["instagram"] as? String)!
        popOverVC.titleshop = (Temp?["cierre_lunes"] as? String)!
        self.logo = (dict["logo"] as? String)!
        
        
        self.present(popOverVC, animated: true , completion: nil)
    }
    
    func buttonAction2(sender: UIButton) {
        
       
      

        // Now if you need to access the selected cell instead of just the index path, you could easily do so by using the table view's cellForRow method
        

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var popOverVC = storyboard.instantiateViewController(withIdentifier: "sbPopUpID2") as! DetailViewController

        
        popOverVC.tel = "12"

    
        
        
        self.present(popOverVC, animated: true , completion: nil)
    }
    

    
    @IBAction func show(_ sender: Any) {
        choosecategories.show()
        
    }
    
    
    
    func Data2(getid: String) {
        
        self.startAnimating(type: NVActivityIndicatorType.ballGridBeat)
        self.tableshop.reloadData()
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.request(self.getAPI.getShopinfo+getid).responseJSON{ (responseData) -> Void in
            if((responseData.result.value) != nil) {
                
                let swiftvar = JSON(responseData.result.value!)
                
                if let jsonvar = swiftvar.arrayObject{
                    self.arrRes = jsonvar as! [[String:AnyObject]]
                    
                    print(responseData)
                    
                }
                if self.arrRes.count > 0{
                    self.tableshop.reloadData()
                    self.stopAnimating()
                }
                
            }
        }
        
    }
    func Data(id:String) {
        
        self.startAnimating(type: NVActivityIndicatorType.ballGridBeat)
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.request(Router.getShopinfo(id)).responseJSON{ (responseData) -> Void in
            if((responseData.result.value) != nil) {
                
                let swiftvar = JSON(responseData.result.value!)
                
                if let jsonvar = swiftvar.arrayObject{
                    self.arrRes = jsonvar as! [[String:AnyObject]]
                    
                    print(responseData)
                    
                }
                if self.arrRes.count > 0{
                    self.tableshop.reloadData()
                    self.stopAnimating()
                }
                
            }
        }
        
    }
    
    func Data3() {
        
        self.startAnimating(type: NVActivityIndicatorType.ballGridBeat)
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.request(Router.getshopCategories()).responseJSON{ (responseData) -> Void in
            if((responseData.result.value) != nil) {
                self.stopAnimating()
                let swiftvar = JSON(responseData.result.value!)
                
                
                
                if let jsonvar = swiftvar.arrayObject{
                    self.arrRes = jsonvar as! [[String:AnyObject]]
                    for s in self.arrRes{
                        
                        
                        self.messageList.append( Categories.init(namecategories: s["nombre"] as! String, id: s["id"] as! Int))
                        
                        self.button1.setTitle(self.messageList.first?.namecategories as! String+"  ", for: .normal)
                        self.Data(id: (self.messageList.first?.id.description)!)
                        
                        self.setupChoose1(categories:self.messageList )
                        
                        
                    }
                    
                    
                }
            }
            
        }
        
        
        
        
    }
}

