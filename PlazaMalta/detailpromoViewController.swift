//
//  detailpromoViewController.swift
//  PlazaMalta
//
//  Created by alex on 9/23/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class detailpromoViewController: UIViewController, NVActivityIndicatorViewable{
    var titlepromo = String(), descrippromo = String(), imagelogo = String()
    
    @IBOutlet weak var line: UIView!
    @IBOutlet weak var imagepromo: UIImageView!
    
    
    @IBOutlet weak var descriptiontext: UITextView!
    @IBOutlet weak var titlepromos: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       

        print(imagelogo)
        
        //titlepromos.text = titlepromo
        //descriptiontext.text = descrippromo
        
        loadImageFromUrl(url: imagelogo, view: imagepromo)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    
    
    @IBAction func goBack(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
        
    }
    
    func loadImageFromUrl(url: String, view: UIImageView){
       self.startAnimating(type: NVActivityIndicatorType.cubeTransition)
        // Create Url from string
        let url = NSURL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = URLSession.shared.dataTask(with: url as URL) { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                 
                // execute in UI thread
                DispatchQueue.main.async(execute: { () -> Void in
                    
                    view.image = UIImage(data: data)
                    self.stopAnimating()
                })
            }
        }
        
        // Run task
        task.resume()
    }

}
