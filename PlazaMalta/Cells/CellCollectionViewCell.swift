//
//  CellCollectionViewCell.swift
//  PlazaMalta
//
//  Created by alex on 9/14/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit

class CellCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var title: UILabel!
  
    override func awakeFromNib() {
        
    }
    
    
}
