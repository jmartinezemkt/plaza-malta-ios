//
//  EventosTableViewCell.swift
//  PlazaMalta
//
//  Created by alex on 9/8/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit

class EventosTableViewCell: UITableViewCell {
    @IBOutlet weak var date: UILabel!

    @IBOutlet weak var eventimage: UIImageView!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
