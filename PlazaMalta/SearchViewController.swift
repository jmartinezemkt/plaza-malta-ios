//
//  ViewController.swift
//  searchBar
//
//  Created by Aashish on 06/01/17.
//  Copyright © 2017 JMataji. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import Kingfisher
import SwiftDate
import Swift

class SearchViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate , NVActivityIndicatorViewable{
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    var searching:Bool! = false
    var searchController = UISearchController(searchResultsController: nil)
    var imgURL: NSURL!
    var arrRes = [[String:AnyObject]]()
    var logo:String = "",closeshop:String = ""
    var currently:Date!, dateopen:Date!
    var dateclose:Date!
    let dates = DateInRegion()
    let dateFormatter = DateFormatter()
    let today = NSDate()
    var estate = Bool()
    let getURL = callUrl()
    
    @IBOutlet weak var open: UIBarButtonItem!
    var items = [GetCategories]()
    var filterItems = [GetCategories]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.showsCancelButton = false
        searchBar.placeholder = "Escriba el nombre de una tienda"

        searchBar.returnKeyType = .google

        General()
        searchBar.delegate = self
     
    }
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.endEditing(true)
        self.searching = true
    }
 
    override func viewDidAppear(_ animated: Bool) {
        if Connectivity.isConnectedToInternet() {
            Data2()
            
            // do some tasks..
        }else{
            
            self.stopAnimating()
        }
    }
    
    func General(){
        if self.revealViewController() != nil {
            self.open.target = self.revealViewController()
            self.open.action = "revealToggle:"
            
        }

        let searchImage = UIImage(named:"logoh.png")?.withRenderingMode(.alwaysOriginal)
        searchBar.delegate = self
        
        
        let searchButton = UIBarButtonItem(image: searchImage,  style: .plain, target: self, action: nil)
        
        navigationItem.leftBarButtonItems = [self.open, searchButton]
        
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackOpaque
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        let color1 = GeneralAttributes.hexStringToUIColor(hex: "#25333F")
        let color3 = GeneralAttributes.hexStringToUIColor(hex: "#2DBEBB")
        
        self.navigationController?.navigationBar.barTintColor = color1;
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: color3]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as! [String : Any]
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.startAnimating(type: NVActivityIndicatorType.ballGridBeat)
        if items.count > 0 {
            self.stopAnimating()
            return 1
        }else {
            TableViewHelper.EmptyMessage2(message: "Cargando...", viewController: self, table: tableView)
            return 0
        }
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searching == true
        {
            
            return filterItems.count
            
            
        }
        else
        {
            return items.count
        }
        
        
    }
    
    //
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as?
            TiendasTableViewCell else{
                fatalError("La instancia de la celda no funciona")
        }
         if searching == true
        {
cell.descriptionshop.scrollRangeToVisible(NSMakeRange(0,0))
            imgURL = NSURL(string: filterItems[indexPath.row].logo as! String)
            cell.descriptionshop.text = filterItems[indexPath.row].descrip as! String
            if imgURL != nil {
                
                cell.imagesshop.kf.setImage(with: imgURL! as URL)
            }
            cell.button.tag = indexPath.row
            cell.button.addTarget(self,action:#selector(buttonAction),
                                  for:.touchUpInside)
            searchBar.selectedScopeButtonIndex = indexPath.row
            let color2 = GeneralAttributes.hexStringToUIColor(hex: "#228c00")
            let color1 = GeneralAttributes.hexStringToUIColor(hex: "#FF2626")
            if cell.hour.text == "ABIERTO"{
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.clock.image = cell.clock.image!.withRenderingMode(.alwaysTemplate)
                cell.clock.tintColor =  color2
                cell.hour.textColor = color2
            }else{
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.clock.image = cell.clock.image!.withRenderingMode(.alwaysTemplate)
                cell.clock.tintColor =  color1
                cell.hour.textColor = color1
            }
        }
        else
        {
            cell.descriptionshop.scrollRangeToVisible(NSMakeRange(0,0))
            imgURL = NSURL(string: items[indexPath.row].logo as! String)
            cell.button.tag = indexPath.row
            cell.button.addTarget(self,action:#selector(buttonAction),
                                  for:.touchUpInside)
            cell.descriptionshop.text = items[indexPath.row].descrip as! String
            if imgURL != nil {
                
                cell.imagesshop.kf.setImage(with: imgURL! as URL)
            }
            else{
                let image = UIImage(named: "store3")!
                cell.imagesshop.image = image
            }

            self.dateFormatter.dateFormat = "EEEE"
            self.dateFormatter.locale = Locale(identifier: "en_US")
            let dayOfWeekString = self.dateFormatter.string(from: today as Date)
            
            switch dayOfWeekString {
                
                /*-------------Show for Monday*/
            case "Monday":
                
                if let Temp = items[indexPath.row].horarios as? NSDictionary {
                    
                    
                    if let close = Temp["cierre_lunes"] as? String{
                        
                        self.closeshop = close
                        
                        /* Get current hour from json*/
                        
                        self.dateFormatter.dateFormat = "HH:mm"
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.dateclose = dateFormatter.date(from: close)!
                        
                        /* ------------------------------*/
                        
                        
                    }
                    
                    if let open = Temp["apertura_lunes"] as? String
                        
                    {
                        
                        /* Get current hour from json*/
                        
                        self.dateFormatter.dateFormat = "HH:mm"
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.dateopen = dateFormatter.date(from: open)!
                        
                        /* ------------------------------*/
                        
                        
                        /* Get current hour*/
                        
                        var str = dates.string(format: .custom("HH:mm")) // example output:
                        
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.currently = dateFormatter.date(from: str)
                        
                        
                        /* ------------------------------*/
                        
                        
                        
                        // Compare them and show
                        switch self.currently.compare(self.dateopen ) {
                        case .orderedAscending     :
                            if(currently < dateopen || currently > dateclose){
                                let components = Calendar.current.dateComponents([.month,.day,.hour,.minute,.second], from: self.dateopen, to: self.currently)
                                
                                let a = Int(components.hour!)
                                let showhour = (a < 0 ? -a : a);
                                if(showhour > 0){
                                    cell.hour.text = "Abre en \(showhour) hrs."
                                    
                                }else if(showhour == 0){
                                    cell.hour.text = "Abre en 1 hrs."
                                }
                                
                                
                            }
                            print("Date A is earlier than date B")
                            break
                            
                        case .orderedDescending    :
                            
                            print("",currently,dateopen, dateclose)
                            if(currently >= dateopen && currently <= dateclose){
                                cell.hour.text = "ABIERTO"
                            }else if(currently < dateopen || currently > dateclose){
                                
                                let components = Calendar.current.dateComponents([.minute,.hour], from: self.dateopen, to: self.currently)
                                let a = Int(components.hour!)
                                
                                let str2 = "01:00"
                                dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                                
                                let comparision2 = dateFormatter.date(from: str2)
                                
                                
                                if (self.currently! > dateclose ){
                                    cell.hour.text = "Faltan \(a)"
                                    
                                }
                                    
                                else if(comparision2! == currently && comparision2! < dateopen){
                                    var c:Int = -8
                                    print(abs(c))
                                    
                                    let components = Calendar.current.dateComponents([.minute,.hour], from: self.currently, to: self.dateopen)
                                    let a = Int(components.hour!)
                                    let str = "\(a)"
                                    cell.hour.text = "Faltan"
                                    
                                    
                                }
                                
                            }
                            print("Date A is later than date B")
                            break
                            
                        case .orderedSame         :
                            cell.hour.text = "ABIERTO"
                            
                        default:
                            
                            cell.hour.text = ""
                            
                            break
                            
                        }
                        
                    }
                    
                }
                
                
                
                break
                
                /*-------------Show for Tuesday*/
            case "Tuesday":
                
                if let Temp = items[indexPath.row].horarios as? NSDictionary {
                    
                    
                    if let close = Temp["cierre_martes"] as? String{
                        
                        self.closeshop = close
                        
                        /* Get current hour from json*/
                        
                        self.dateFormatter.dateFormat = "HH:mm"
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.dateclose = dateFormatter.date(from: close)!
                        
                        /* ------------------------------*/
                        
                        
                    }
                    
                    if let open = Temp["apertura_martes"] as? String
                        
                    {
                        
                        /* Get current hour from json*/
                        
                        self.dateFormatter.dateFormat = "HH:mm"
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.dateopen = dateFormatter.date(from: open)!
                        
                        /* ------------------------------*/
                        
                        
                        /* Get current hour*/
                        
                        var str = dates.string(format: .custom("HH:mm")) // example output:
                        
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.currently = dateFormatter.date(from: str)
                        
                        
                        /* ------------------------------*/
                        
                        
                        
                        // Compare them and show
                        switch self.currently.compare(self.dateopen ) {
                        case .orderedAscending     :
                            if(currently < dateopen || currently > dateclose){
                                let components = Calendar.current.dateComponents([.month,.day,.hour,.minute,.second], from: self.dateopen, to: self.currently)
                                
                                let a = Int(components.hour!)
                                let showhour = (a < 0 ? -a : a);
                                if(showhour > 0){
                                    cell.hour.text = "Abre en \(showhour) hrs."
                                    
                                }else if(showhour == 0){
                                    cell.hour.text = "Abre en 1 hrs."
                                }
                                
                                
                            }
                            print("Date A is earlier than date B")
                            break
                            
                        case .orderedDescending    :
                            
                            print("",currently,dateopen, dateclose)
                            if(currently >= dateopen && currently <= dateclose){
                                cell.hour.text = "ABIERTO"
                            }else if(currently < dateopen || currently > dateclose){
                                
                                let components = Calendar.current.dateComponents([.minute,.hour], from: self.dateopen, to: self.currently)
                                let a = Int(components.hour!)
                                
                                let str2 = "01:00"
                                dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                                
                                let comparision2 = dateFormatter.date(from: str2)
                                
                                
                                if (self.currently! > dateclose ){
                                    cell.hour.text = "Faltan \(a)"
                                    
                                }
                                    
                                else if(comparision2! == currently && comparision2! < dateopen){
                                    var c:Int = -8
                                    print(abs(c))
                                    
                                    let components = Calendar.current.dateComponents([.minute,.hour], from: self.currently, to: self.dateopen)
                                    let a = Int(components.hour!)
                                    let str = "\(a)"
                                    cell.hour.text = "Faltan"
                                    
                                    
                                }
                                
                            }
                            print("Date A is later than date B")
                            break
                            
                        case .orderedSame         :
                            cell.hour.text = "ABIERTO"
                            
                        default:
                            
                            cell.hour.text = ""
                            
                            break
                            
                        }
                        
                    }
                    
                }
                
                
                
                
                
                break
                
                /*-------------Show for Wednesday*/
                
            case "Wednesday":
                
                if let Temp = items[indexPath.row].horarios as? NSDictionary {
                    
                    
                    if let close = Temp["cierre_miercoles"] as? String{
                        
                        self.closeshop = close
                        
                        /* Get current hour from json*/
                        
                        self.dateFormatter.dateFormat = "HH:mm"
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.dateclose = dateFormatter.date(from: close)!
                        
                        /* ------------------------------*/
                        
                        
                    }
                    
                    if let open = Temp["apertura_miercoles"] as? String
                        
                    {
                        
                        /* Get current hour from json*/
                        
                        self.dateFormatter.dateFormat = "HH:mm"
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.dateopen = dateFormatter.date(from: open)!
                        
                        /* ------------------------------*/
                        
                        
                        /* Get current hour*/
                        
                        var str = dates.string(format: .custom("HH:mm")) // example output:
                        
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.currently = dateFormatter.date(from: str)
                        
                        
                        /* ------------------------------*/
                        
                        
                        
                        // Compare them and show
                        switch self.currently.compare(self.dateopen ) {
                        case .orderedAscending     :
                            if(currently < dateopen || currently > dateclose){
                                let components = Calendar.current.dateComponents([.month,.day,.hour,.minute,.second], from: self.dateopen, to: self.currently)
                                
                                let a = Int(components.hour!)
                                let showhour = (a < 0 ? -a : a);
                                if(showhour > 0){
                                    cell.hour.text = "Abre en \(showhour) hrs."
                                    
                                }else if(showhour == 0){
                                    cell.hour.text = "Abre en 1 hrs."
                                }
                                
                                
                            }
                            print("Date A is earlier than date B")
                            break
                            
                        case .orderedDescending    :
                            
                            print("",currently,dateopen, dateclose)
                            if(currently >= dateopen && currently <= dateclose){
                                cell.hour.text = "ABIERTO"
                            }else if(currently < dateopen || currently > dateclose){
                                
                                let components = Calendar.current.dateComponents([.minute,.hour], from: self.dateopen, to: self.currently)
                                let a = Int(components.hour!)
                                
                                let str2 = "01:00"
                                dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                                
                                let comparision2 = dateFormatter.date(from: str2)
                                
                                
                                if (self.currently! > dateclose ){
                                    cell.hour.text = "Faltan \(a)"
                                    
                                }
                                    
                                else if(comparision2! == currently && comparision2! < dateopen){
                                    var c:Int = -8
                                    print(abs(c))
                                    
                                    let components = Calendar.current.dateComponents([.minute,.hour], from: self.currently, to: self.dateopen)
                                    let a = Int(components.hour!)
                                    let str = "\(a)"
                                    cell.hour.text = "Faltan"
                                    
                                    
                                }
                                
                            }
                            print("Date A is later than date B")
                            break
                            
                        case .orderedSame         :
                            cell.hour.text = "ABIERTO"
                            
                        default:
                            
                            cell.hour.text = ""
                            
                            break
                            
                        }
                        
                    }
                    
                }
                
                
                
                
                break
                
                /*-------------Show for Thursday*/
            case "Thursday":
                if let Temp = items[indexPath.row].horarios as? NSDictionary {
                    
                    
                    if let close = Temp["cierre_jueves"] as? String{
                        
                        self.closeshop = close
                        
                        /* Get current hour from json*/
                        
                        self.dateFormatter.dateFormat = "HH:mm"
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.dateclose = dateFormatter.date(from: close)!
                        
                        /* ------------------------------*/
                        
                        
                    }
                    
                    if let open = Temp["apertura_jueves"] as? String
                        
                    {
                        
                        /* Get current hour from json*/
                        
                        self.dateFormatter.dateFormat = "HH:mm"
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.dateopen = dateFormatter.date(from: open)!
                        
                        /* ------------------------------*/
                        
                        
                        /* Get current hour*/
                        
                        var str = dates.string(format: .custom("HH:mm")) // example output:
                        
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.currently = dateFormatter.date(from: str)
                        
                        
                        /* ------------------------------*/
                        
                        
                        
                        // Compare them and show
                        switch self.currently.compare(self.dateopen ) {
                        case .orderedAscending     :
                            if(currently < dateopen || currently > dateclose){
                                let components = Calendar.current.dateComponents([.month,.day,.hour,.minute,.second], from: self.dateopen, to: self.currently)
                                
                                let a = Int(components.hour!)
                                let showhour = (a < 0 ? -a : a);
                                if(showhour > 0){
                                    cell.hour.text = "Abre en \(showhour) hrs."
                                    
                                }else if(showhour == 0){
                                    cell.hour.text = "Abre en 1 hrs."
                                }
                                
                                
                            }
                            print("Date A is earlier than date B")
                            break
                            
                        case .orderedDescending    :
                            
                            print("",currently,dateopen, dateclose)
                            if(currently >= dateopen && currently <= dateclose){
                                cell.hour.text = "ABIERTO"
                            }else if(currently < dateopen || currently > dateclose){
                                
                                let components = Calendar.current.dateComponents([.minute,.hour], from: self.dateopen, to: self.currently)
                                let a = Int(components.hour!)
                                
                                let str2 = "01:00"
                                dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                                
                                let comparision2 = dateFormatter.date(from: str2)
                                
                                
                                if (self.currently! > dateclose ){
                                    cell.hour.text = "Faltan \(a)"
                                    
                                }
                                    
                                else if(comparision2! == currently && comparision2! < dateopen){
                                    var c:Int = -8
                                    print(abs(c))
                                    
                                    let components = Calendar.current.dateComponents([.minute,.hour], from: self.currently, to: self.dateopen)
                                    let a = Int(components.hour!)
                                    let str = "\(a)"
                                    cell.hour.text = "Faltan"
                                    
                                    
                                }
                                
                            }
                            print("Date A is later than date B")
                            break
                            
                        case .orderedSame         :
                            cell.hour.text = "ABIERTO"
                            
                        default:
                            
                            cell.hour.text = ""
                            
                            break
                            
                        }
                        
                    }
                    
                }
                
                
                
                
                break
                
                /*-------------Show for Friday*/
            case "Friday":
                if let Temp = items[indexPath.row].horarios as? NSDictionary {
                    
                    
                    if let close = Temp["cierre_viernes"] as? String{
                        
                        self.closeshop = close
                        
                        /* Get current hour from json*/
                        
                        self.dateFormatter.dateFormat = "HH:mm"
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.dateclose = dateFormatter.date(from: close)!
                        
                        /* ------------------------------*/
                        
                        
                    }
                    
                    if let open = Temp["apertura_viernes"] as? String
                        
                    {
                        
                        /* Get current hour from json*/
                        
                        self.dateFormatter.dateFormat = "HH:mm"
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.dateopen = dateFormatter.date(from: open)!
                        
                        /* ------------------------------*/
                        
                        
                        /* Get current hour*/
                        
                        var str = dates.string(format: .custom("HH:mm")) // example output:
                        
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.currently = dateFormatter.date(from: str)
                        
                        
                        /* ------------------------------*/
                        
                        
                        
                        // Compare them and show
                        switch self.currently.compare(self.dateopen ) {
                        case .orderedAscending     :
                            if(currently < dateopen || currently > dateclose){
                                let components = Calendar.current.dateComponents([.month,.day,.hour,.minute,.second], from: self.dateopen, to: self.currently)
                                
                                let a = Int(components.hour!)
                                let showhour = (a < 0 ? -a : a);
                                if(showhour > 0){
                                    cell.hour.text = "Abre en \(showhour) hrs."
                                    
                                }else if(showhour == 0){
                                    cell.hour.text = "Abre en 1 hrs."
                                }
                                
                                
                            }
                            print("Date A is earlier than date B")
                            break
                            
                        case .orderedDescending    :
                            
                            print("",currently,dateopen, dateclose)
                            if(currently >= dateopen && currently <= dateclose){
                                cell.hour.text = "ABIERTO"
                            }else if(currently < dateopen || currently > dateclose){
                                
                                let components = Calendar.current.dateComponents([.minute,.hour], from: self.dateopen, to: self.currently)
                                let a = Int(components.hour!)
                                
                                let str2 = "01:00"
                                dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                                
                                let comparision2 = dateFormatter.date(from: str2)
                                
                                
                                if (self.currently! > dateclose ){
                                    cell.hour.text = "Faltan \(a)"
                                    
                                }
                                    
                                else if(comparision2! == currently && comparision2! < dateopen){
                                    var c:Int = -8
                                    print(abs(c))
                                    
                                    let components = Calendar.current.dateComponents([.minute,.hour], from: self.currently, to: self.dateopen)
                                    let a = Int(components.hour!)
                                    let str = "\(a)"
                                    cell.hour.text = "Faltan"
                                    
                                    
                                }
                                
                            }
                            print("Date A is later than date B")
                            break
                            
                        case .orderedSame         :
                            cell.hour.text = "ABIERTO"
                            
                        default:
                            
                            cell.hour.text = ""
                            
                            break
                            
                        }
                        
                    }
                    
                }
                
                
                
                
                break
                
                
                
                /*-------------Show for Saturday*/
            case "Saturday":
                if let Temp = items[indexPath.row].horarios as? NSDictionary {
                    
                    
                    if let close = Temp["cierre_sabado"] as? String{
                        
                        self.closeshop = close
                        
                        /* Get current hour from json*/
                        
                        self.dateFormatter.dateFormat = "HH:mm"
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.dateclose = dateFormatter.date(from: close)!
                        
                        /* ------------------------------*/
                        
                        
                    }
                    
                    if let open = Temp["apertura_sabado"] as? String
                        
                    {
                        
                        /* Get current hour from json*/
                        
                        self.dateFormatter.dateFormat = "HH:mm"
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.dateopen = dateFormatter.date(from: open)!
                        
                        /* ------------------------------*/
                        
                        
                        /* Get current hour*/
                        
                        var str = dates.string(format: .custom("HH:mm")) // example output:
                        
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.currently = dateFormatter.date(from: str)
                        
                        
                        /* ------------------------------*/
                        
                        
                        
                        // Compare them and show
                        switch self.currently.compare(self.dateopen ) {
                        case .orderedAscending     :
                            if(currently < dateopen || currently > dateclose){
                                let components = Calendar.current.dateComponents([.month,.day,.hour,.minute,.second], from: self.dateopen, to: self.currently)
                                
                                let a = Int(components.hour!)
                                let showhour = (a < 0 ? -a : a);
                                if(showhour > 0){
                                    cell.hour.text = "Abre en \(showhour) hrs."
                                    
                                }else if(showhour == 0){
                                    cell.hour.text = "Abre en 1 hrs."
                                }
                                
                                
                            }
                            print("Date A is earlier than date B")
                            break
                            
                        case .orderedDescending    :
                            
          
                            if(currently >= dateopen && currently <= dateclose){
                                cell.hour.text = "ABIERTO"
                            }else if(currently < dateopen || currently > dateclose){
                                
                                let components = Calendar.current.dateComponents([.minute,.hour], from: self.dateopen, to: self.currently)
                                let a = Int(components.hour!)
                                
                                let str2 = "01:00"
                                dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                                
                                let comparision2 = dateFormatter.date(from: str2)
                                
                                
                                if (self.currently! > dateclose ){
                                    cell.hour.text = "Faltan \(a)"
                                    
                                }
                                    
                                else if(comparision2! == currently && comparision2! < dateopen){
                                    var c:Int = -8
                                    print(abs(c))
                                    
                                    let components = Calendar.current.dateComponents([.minute,.hour], from: self.currently, to: self.dateopen)
                                    let a = Int(components.hour!)
                                    let str = "\(a)"
                                    cell.hour.text = "Faltan"
                                    
                                    
                                }
                                
                            }
                            print("Date A is later than date B")
                            break
                            
                        case .orderedSame         :
                            cell.hour.text = "ABIERTO"
                            
                        default:
                            
                            cell.hour.text = ""
                            
                            break
                            
                        }
                        
                    }
                    
                }
                
                
                
                
                break
                
                /*-------------Show for Sunday*/
            case "Sunday":
                if let Temp = items[indexPath.row].horarios as? NSDictionary {
                    
                    
                    if var close = Temp["cierre_domingo"] as? String{
                        
                        if close != ""{
                            self.closeshop = close
                        } else{
                            closeshop = "00:00"
                            close = "00:00"
                            
                        }
                        
                        /* Get current hour from json*/
                        
                        self.dateFormatter.dateFormat = "HH:mm"
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.dateclose = dateFormatter.date(from: close)!
                        
                        /* ------------------------------*/
                        
                        
                    }
                    
                    if var open = Temp["apertura_domingo"] as? String
                        
                    {
                        if open == ""{
                            open = "00:00"
                            
                        }
                        /* Get current hour from json*/
                        
                        self.dateFormatter.dateFormat = "HH:mm"
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.dateopen = dateFormatter.date(from: open)!
                        
                        /* ------------------------------*/
                        
                        
                        /* Get current hour*/
                        
                        var str = dates.string(format: .custom("HH:mm")) // example output:
                        
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                        self.currently = dateFormatter.date(from: str)
                        
                        
                        /* ------------------------------*/
                        
                        
                        
                        // Compare them and show
                        switch self.currently.compare(self.dateopen ) {
                        case .orderedAscending     :
                            if(currently < dateopen || currently > dateclose){
                                let components = Calendar.current.dateComponents([.month,.day,.hour,.minute,.second], from: self.dateopen, to: self.currently)
                                
                                let a = Int(components.hour!)
                                let showhour = (a < 0 ? -a : a);
                                if(showhour > 0){
                                    cell.hour.text = "Abre en \(showhour) hrs."
                                    
                                }else if(showhour == 0){
                                    cell.hour.text = "Abre en 1 hrs."
                                }
                                
                                
                            }
                            print("Date A is earlier than date B")
                            break
                            
                        case .orderedDescending    :
                            
                            print("",currently,dateopen, dateclose)
                            if(currently >= dateopen && currently <= dateclose){
                                cell.hour.text = "ABIERTO"
                            }else if(currently < dateopen || currently > dateclose){
                                
                                let components = Calendar.current.dateComponents([.minute,.hour], from: self.dateopen, to: self.currently)
                                let a = Int(components.hour!)
                                
                                let str2 = "01:00"
                                dateFormatter.timeZone = TimeZone(identifier: "UTC")!
                                
                                let comparision2 = dateFormatter.date(from: str2)
                                
                                
                                if (self.currently! > dateclose ){
                                    cell.hour.text = "Faltan \(a)"
                                    
                                }
                                    
                                else if(comparision2! == currently && comparision2! < dateopen){
                                    var c:Int = -8
                                    print(abs(c))
                                    
                                    let components = Calendar.current.dateComponents([.minute,.hour], from: self.currently, to: self.dateopen)
                                    let a = Int(components.hour!)
                                    let str = "\(a)"
                                    cell.hour.text = "Faltan"
                                    
                                    
                                }
                                
                            }
                            print("Date A is later than date B")
                            break
                            
                        case .orderedSame         :
                            cell.hour.text = "ABIERTO"
                            
                        default:
                            
                            cell.hour.text = "ABIERTO"
                            
                            break
                            
                        }
                        
                    }
                    
                }
                
                
                
                
                break
                
                
                
            default:
                cell.hour.text = "ABIERTO"
                break
            
            
        }
        
        }
        let color2 = GeneralAttributes.hexStringToUIColor(hex: "#228c00")
        let color1 = GeneralAttributes.hexStringToUIColor(hex: "#228c00")
        if cell.hour.text == "ABIERTO"{
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.clock.image = cell.clock.image!.withRenderingMode(.alwaysTemplate)
            cell.clock.tintColor =  color2
            cell.hour.textColor = color2
        }else{
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.clock.image = cell.clock.image!.withRenderingMode(.alwaysTemplate)
            cell.clock.tintColor =  color1
            cell.hour.textColor = color1
        }
        
        return cell
        
    }
        
    

    
    func buttonAction(sender: UIButton) {
        if searching == true{
          
            
            
            let selectedIndex = IndexPath(row: sender.tag, section: 0)
            var dict = filterItems[(selectedIndex as NSIndexPath).row]
            var dict2 = arrRes[(selectedIndex as NSIndexPath).row]
            let indexPath = self.tableView.indexPathForSelectedRow;
            // And finally do whatever you need using this index :
            
            self.tableView.selectRow(at: selectedIndex, animated: true, scrollPosition: .none)
            
            // Now if you need to access the selected cell instead of just the index path, you could easily do so by using the table view's cellForRow method
            
            let selectedCell = tableView.cellForRow(at: selectedIndex) as! TiendasTableViewCell
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            var popOverVC = storyboard.instantiateViewController(withIdentifier: "sbPopUpID2") as! DetailViewController
            
            
            
            
            // popOverVC.logo = sel
            
            popOverVC.logo =  dict.logo.description
            
            let Temp = dict.horarios.description
    
            
            popOverVC.email = dict.email.description
            popOverVC.tel = dict.telefono.description
            popOverVC.telshop2 = dict.telefono2.description
            popOverVC.facebook = dict.facebook.description
            popOverVC.twitter = dict.twitter.description
            popOverVC.instagram = dict.instagram.description
            popOverVC.getfood = dict.menu.description
            if let Temp = dict2["horarios"] as? NSDictionary {
                
                if let close = Temp["cierre_lunes"] as? String{
                    popOverVC.cp1 = close.description
                }
                
                if let open = Temp["apertura_lunes"] as? String
                    
                {
                    popOverVC.op1 = open.description
                    
                    
                }
                
            }
            if let Temp = dict2["horarios"] as? NSDictionary {
                
                if let close = Temp["cierre_martes"] as? String{
                    popOverVC.cp2 = close.description
                }
                
                if let open = Temp["apertura_martes"] as? String
                    
                {
                    popOverVC.op2 = open.description
                    
                    
                }
                
            }
            
            if let Temp = dict2["horarios"] as? NSDictionary {
                
                if let close = Temp["cierre_miercoles"] as? String{
                    popOverVC.cp3 = close.description
                }
                
                if let open = Temp["apertura_miercoles"] as? String
                    
                {
                    popOverVC.op3 = open.description
                    
                    
                }
                
            }
            if let Temp = dict2["horarios"] as? NSDictionary {
                
                if let close = Temp["cierre_jueves"] as? String{
                    popOverVC.cp4 = close.description
                }
                
                if let open = Temp["apertura_jueves"] as? String
                    
                {
                    popOverVC.op4 = open.description
                    
                    
                }
                
            }
            if let Temp = dict2["horarios"] as? NSDictionary {
                
                if let close = Temp["cierre_viernes"] as? String{
                    popOverVC.cp5 = close.description
                }
                
                if let open = Temp["apertura_viernes"] as? String
                    
                {
                    popOverVC.op5 = open.description
                    
                    
                }
                
            }
            if let Temp = dict2["horarios"] as? NSDictionary {
                
                if let close = Temp["cierre_sabado"] as? String{
                    popOverVC.cp6 = close.description
                }
                
                if let open = Temp["apertura_sabado"] as? String
                    
                {
                    popOverVC.op6 = open.description
                    
                    
                }
                
            }
            
            
            
            if let Temp = dict2["horarios"] as? NSDictionary {
                
                
                if let close = Temp["cierre_domingo"] as? String{
                    popOverVC.cp7 = close.description
                    
                    
                }
                
                if let open = Temp["apertura_domingo"] as? String{
                    popOverVC.op7 = open.description
   
                }
                
            }
            
            
            self.present(popOverVC, animated: true , completion: nil)
            
            
        }else{
            let selectedIndex = IndexPath(row: sender.tag, section: 0)
            var dict = arrRes[(selectedIndex as NSIndexPath).row]
            // Get Cell Label
            let indexPath = tableView.indexPathForSelectedRow;
            // And finally do whatever you need using this index :
            
            
            tableView.selectRow(at: selectedIndex, animated: true, scrollPosition: .none)
            tableView.selectRow(at: selectedIndex, animated: true, scrollPosition: .none)
            // Now if you need to access the selected cell instead of just the index path, you could easily do so by using the table view's cellForRow method
            
            let selectedCell = tableView.cellForRow(at: selectedIndex) as! TiendasTableViewCell
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            var popOverVC = storyboard.instantiateViewController(withIdentifier: "sbPopUpID2") as! DetailViewController
            let Temp = dict["horarios"]
            if let Temp = dict["horarios"] as? NSDictionary {
                
                if let close = Temp["cierre_lunes"] as? String{
                    popOverVC.cp1 = close.description
                }
                
                if let open = Temp["apertura_lunes"] as? String
                    
                {
                    popOverVC.op1 = open.description
                    
                    
                }
                
            }
            if let Temp = dict["horarios"] as? NSDictionary {
                
                if let close = Temp["cierre_martes"] as? String{
                    popOverVC.cp2 = close.description
                }
                
                if let open = Temp["apertura_martes"] as? String
                    
                {
                    popOverVC.op2 = open.description
                    
                    
                }
                
            }
            
            if let Temp = dict["horarios"] as? NSDictionary {
                
                if let close = Temp["cierre_miercoles"] as? String{
                    popOverVC.cp3 = close.description
                }
                
                if let open = Temp["apertura_miercoles"] as? String
                    
                {
                    popOverVC.op3 = open.description
                    
                    
                }
                
            }
            if let Temp = dict["horarios"] as? NSDictionary {
                
                if let close = Temp["cierre_jueves"] as? String{
                    popOverVC.cp4 = close.description
                }
                
                if let open = Temp["apertura_jueves"] as? String
                    
                {
                    popOverVC.op4 = open.description
                    
                    
                }
                
            }
            if let Temp = dict["horarios"] as? NSDictionary {
                
                if let close = Temp["cierre_viernes"] as? String{
                    popOverVC.cp5 = close.description
                }
                
                if let open = Temp["apertura_viernes"] as? String
                    
                {
                    popOverVC.op5 = open.description
                    
                    
                }
                
            }
            if let Temp = dict["horarios"] as? NSDictionary {
                
                if let close = Temp["cierre_sabado"] as? String{
                    popOverVC.cp6 = close.description
                }
                
                if let open = Temp["apertura_sabado"] as? String
                    
                {
                    popOverVC.op6 = open.description
                    
                    
                }
                
            }
            
            
            
            if let Temp = dict["horarios"] as? NSDictionary {
                
                
                if let close = Temp["cierre_domingo"] as? String{
                    popOverVC.cp7 = close.description
                    
                    
                }
                
                if let open = Temp["apertura_domingo"] as? String
                    
                {
                    popOverVC.op7 = open.description
                    
                    
                }
                
            }
            
            popOverVC.logo = (dict["logo"] as? String)!
            
            popOverVC.email = (dict["email"] as? String)!
            popOverVC.tel = (dict["telefono"] as? String)!
            popOverVC.telshop2 = (dict["telefono2"] as? String)!
            popOverVC.title = (Temp?["cierre_lunes"] as? String)!
            popOverVC.facebook = (dict["facebook"] as? String)!
            popOverVC.twitter = (dict["twitter"] as? String)!
            popOverVC.instagram = (dict["instagram"] as? String)!
            popOverVC.titleshop = (Temp?["cierre_lunes"] as? String)!
            popOverVC.getfood = (dict["menu"] as? String)!
            self.logo = (dict["logo"] as? String)!
            
            
            self.present(popOverVC, animated: true , completion: nil)
            
        }
       
    }
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //Looks for single or multiple taps.
        
        filterItems = items.filter({ (text) -> Bool in
            let tmp: NSString = text.name as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)

            return range.location != NSNotFound
        })
        if(filterItems.count == 0){
            
            
                TableViewHelper.EmptyMessage2(message: "No hay resultados...", viewController: self, table: tableView)
          

            
        } else {
            
            searching = true;
           
        }
       
        self.tableView.reloadData()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }

    // this when search ends
    // Now to hide keyboard just write code
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searching = false
        self.view.endEditing(true)
        // it's hide cancel button when search end
        searchBar.showsCancelButton = false
 
    }
    // when search begin
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searching = true
        
        // it's show cancel button when searching begin
        searchBar.showsCancelButton = true
        tableView.reloadData()
       
    }
    // when you click on cancel
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        self.view.endEditing(true)
        searchBar.showsCancelButton = false
        
        tableView.reloadData()
    }
    // when you click on search
   
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        self.searchBar.resignFirstResponder()
        
        self.searching = true
        

    }
  
 
 
    func Data2() {
        
        self.startAnimating(type: NVActivityIndicatorType.ballGridBeat)
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.request(Router.generalShopsinfo()).responseJSON{ (responseData) -> Void in
            if((responseData.result.value) != nil) {
                
                let swiftvar = JSON(responseData.result.value!)
                if let resData = swiftvar.arrayObject as? [[String: Any]] {
                    
                    self.items = resData.map(GetCategories.init)
          
                }
                if let jsonvar = swiftvar.arrayObject{
                    self.arrRes = jsonvar as! [[String:AnyObject]]
          
                }
                
                
                if self.items.count > 0{
              
                    self.tableView.reloadData()
                    self.stopAnimating()
               }
                
            }
        }
        
    }

}
