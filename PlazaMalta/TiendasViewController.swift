 //
//  TiendasViewController.swift
//  PlazaMalta
//
//  Created by alex on 8/24/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit
import XLPagerTabStrip
 

class TiendasViewController: ButtonBarPagerTabStripViewController {

    
    @IBOutlet weak var Scroll: UIScrollView!
    @IBOutlet weak var abrir: UIBarButtonItem!
    
    @IBOutlet weak var nav: UINavigationItem!
    override func viewDidLoad() {
        

        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .black
        settings.style.selectedBarBackgroundColor = .white

        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 3.0
        let color3 = self.hexStringToUIColor(hex: "#2DBEBB")
        settings.style.selectedBarBackgroundColor = color3
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .white
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
       
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .white
            let color1 = self?.hexStringToUIColor(hex: "#2DBEBB")
            newCell?.label.textColor = color1
        
            
        }
        if self.revealViewController() != nil {
            abrir.target = self.revealViewController()
            abrir.action = "revealToggle:"
          
        }
        let color1 = self.hexStringToUIColor(hex: "#25333F")
        let color2 = self.hexStringToUIColor(hex: "#2DBEBB")
        
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        
        self.navigationController?.navigationBar.barTintColor = color1;
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: color2]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as! [String : Any]
       
    super.viewDidLoad()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child1")
        let child_2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child2")
        let child_3 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child3")
        let child_4 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child4")
        let child_5 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child5")
        let child_6 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child6")
        let child_7 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child7")
        let child_8 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child8")
        let child_9 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child9")
        let child_10 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child10")
        return [child_1, child_2, child_3, child_4, child_5,child_6,child_7,child_8,child_9,child_10]
    }
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }



}
