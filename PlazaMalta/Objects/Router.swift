//
//  Router.swift
//  PlazaMalta
//
//  Created by elaniin on 12/19/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
    static let baseURLString = "http://plazamalta.com.sv/wp-json/v1/"
    case getShopinfo(String)
    case getshopCategories()
    case getRestaurantsinfo(String)
    case getRestaurantcategories()
    case generalShopsinfo()
    case getEvents()
    case getPromos()
    
    func asURLRequest() throws -> URLRequest {
        
        var method: HTTPMethod {
            switch self {
            case .getShopinfo:
                return .get
            case .getshopCategories:
                return .get

            case .getRestaurantsinfo:
                return .get
            case .getRestaurantcategories:
                return .get
            case .generalShopsinfo:
                return .get
            case .getEvents:
                return .get
            case .getPromos:
                return .get
            }
        }
        
        let params: ([String: Any]?) = {
            switch self {
            case .getShopinfo:
                return nil
            
            case .getshopCategories:
                return nil
            case .getRestaurantsinfo(_):
                return nil
            case .getRestaurantcategories:
                return nil
            case .generalShopsinfo:
                return nil
            case .getEvents:
                return nil
            case .getPromos:
                return nil
            }
        }()
        
        let url: URL = {
            // build up and return the URL for each endpoint
            let relativePath: String?
            switch self {
            case .getShopinfo(let id):
                relativePath = "tiendas/\(id)"
            
            case .getshopCategories:
                relativePath = "categorias-tiendas"
                
            case .getRestaurantsinfo(let id):
                relativePath = "restaurantes/\(id)"
            case .getRestaurantcategories:
                relativePath = "categorias-restaurantes"
            case .generalShopsinfo:
                relativePath = "all"
            case .getEvents:
                relativePath = "eventos"
            case .getPromos:
                relativePath = "promociones/0"
            }
            var url = URL(string: Router.baseURLString)!
            if let relativePath = relativePath {
                url = url.appendingPathComponent(relativePath)
            }
            return url
        }()
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        
        let encoding = JSONEncoding.default
        return try encoding.encode(urlRequest, with: params)
    }
}
