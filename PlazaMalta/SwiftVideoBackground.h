//
//  SwiftVideoBackground.h
//  PlazaMalta
//
//  Created by alex on 8/23/17.
//  Copyright © 2017 elaniin. All rights reserved.
//
#import <UIKit/UIKit.h>

//! Project version number for SwiftVideoBackground.
FOUNDATION_EXPORT double SwiftVideoBackgroundVersionNumber;

//! Project version string for SwiftVideoBackground.
FOUNDATION_EXPORT const unsigned char SwiftVideoBackgroundVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SwiftVideoBackground/PublicHeader.h>
