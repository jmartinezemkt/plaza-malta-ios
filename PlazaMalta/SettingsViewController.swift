//
//  SettingsViewController.swift
//  PlazaMalta
//
//  Created by alex on 9/14/17.
//  Copyright © 2017 elaniin. All rights reserved.
//

import UIKit

import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import NVActivityIndicatorView

class SettingsViewController:  UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,NVActivityIndicatorViewable  {
    
    @IBOutlet weak var change: UIButton!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var open: UIBarButtonItem!
   
    @IBOutlet weak var hideview: UIView!
    @IBOutlet weak var viewback: UIView!
    
    let storage = Storage.storage().reference()
    let ref = Database.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        General()
setupProfile()
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SettingsViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        let defaults = UserDefaults.standard
        let savetoken = String()
        if let stringOne = defaults.string(forKey: menuTableViewController.defaultsKeys.keyOne) {
            if stringOne != ""{
                print("xx"+stringOne)
                self.hideview.isHidden = true
                self.profile.isHidden = true
            }else{
                self.hideview.isHidden = false
                self.profile.isHidden = false
            }
            
        }
        
        if let stringOne = defaults.string(forKey: menuTableViewController.defaultsKeys.keyTwo) {
            if stringOne != ""{
                print("xx"+stringOne)
                
                self.hideview.isHidden = true
                self.profile.isHidden = true

                
            }else{
                self.hideview.isHidden = false
                self.profile.isHidden = false
                            }
            
        }
        view.addGestureRecognizer(tap)
    }
    
    
    @IBAction func changeimage(_ sender: Any) {
        
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true

        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {
            action in
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: {
            action in
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage{
            selectedImageFromPicker = editedImage
        }else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage{
            selectedImageFromPicker = originalImage
        }
        if let selectedImage = selectedImageFromPicker{
            profile.image = selectedImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //Calls this function when the tap is recognized.
     func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func General(){
        let searchImage = UIImage(named:"logoh.png")?.withRenderingMode(.alwaysOriginal)
        
        
        
        let searchButton = UIBarButtonItem(image: searchImage,  style: .plain, target: self, action: nil)
        
     
        
        navigationItem.leftBarButtonItems = [self.open, searchButton]

        GeneralAttributes.circleimage(imageview: self.profile)
        
    
        let color3 = self.hexStringToUIColor(hex: "#2DBEBB")
        
        if self.revealViewController() != nil {
            open.target = self.revealViewController()
            open.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        let color1 = self.hexStringToUIColor(hex: "#25333F")
        
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackOpaque
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationController?.navigationBar.barTintColor = color1;
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: color3]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as! [String : Any]
        change.layer.borderWidth = 0.0

        change.backgroundColor = UIColor.clear
        change.titleLabel?.layer.masksToBounds = false
        
        let borderBottom = CALayer()
        let borderWidth = CGFloat(2.0)
        borderBottom.borderColor = UIColor.gray.cgColor
        borderBottom.frame = CGRect(x: 0, y: username.frame.height - 1.0, width: username.frame.width , height: username.frame.height - 1.0)
        borderBottom.borderWidth = borderWidth
        username.layer.addSublayer(borderBottom)
        username.layer.masksToBounds = true
        username.tintColor = .white
    }
    
    
    
    func setupProfile(){
        
        
        
        if let uid = Auth.auth().currentUser?.uid{
            ref.child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                if let dict = snapshot.value as? [String: AnyObject]
                {
                    self.username.text = dict["username"] as? String
                    
                    if let profileImageURL = dict["photo"] as? String
                    {
                        let url = URL(string: profileImageURL)
                        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                            if error != nil{
                                print(error!)
                                return
                            }
                            DispatchQueue.main.async {
                                self.profile?.image = UIImage(data: data!)
                            }
                        }).resume()
                    }
                }
            })
            
        }
    }
    
    
    @IBAction func save(_ sender: Any) {
        saveChanges()
       
    }
    
 
    func saveChanges(){
        self.startAnimating()
        let imageName = NSUUID().uuidString
        
        let storedImage = storage.child("profile_images").child(imageName)
        
        if let uploadData = UIImagePNGRepresentation(self.profile.image!)
        {
            storedImage.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                if error != nil{
                    print(error!)
                    return
                }
                storedImage.downloadURL(completion: { (url, error) in
                    if error != nil{
                        print(error!)
                        return
                    }
                    if let urlText = url?.absoluteString{
                        if (urlText != "" && self.username.text! != "" ){
                        self.ref.child("users").child((Auth.auth().currentUser?.uid)!).updateChildValues(["photo" : urlText,"username": self.username.text!], withCompletionBlock: { (error, ref) in
                            if error != nil{
                                print(error!)
                                return
                            }else{
                                self.stopAnimating()
                                let alert = UIAlertController(title: "Actualizado ", message: "Los cambios fueron aplicados a su perfil correctamente", preferredStyle: UIAlertControllerStyle.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)

                            }
                            
                        })                   }else{
                            let alert = UIAlertController(title: "No se pudo actualizar ", message: "Verifique que sus campos no esten vacios", preferredStyle: UIAlertControllerStyle.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)

                        }}
                })
            })
        }
    }
    
    @IBAction func change(_ sender: Any) {
       changealert()
    }
 
    
    
    func changealert(){
        let refreshAlert = UIAlertController(title: "Cambiar Mi Contraseña", message: "Recibira un link en su cuenta de correo", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Cambiar", style: .default, handler: { (action: UIAlertAction!) in
            var user = Auth.auth().currentUser
            let email = "" + (user?.email!)!
            Auth.auth().sendPasswordReset(withEmail: email) { (error) in
                if(error != nil){
                    print("Error")
                }else{
                    let alert = UIAlertController(title: "Mensaje Enviado ", message: "Su Contraseña no cambiara, mientras usted no haga el cambio en el link enviado", preferredStyle: UIAlertControllerStyle.alert)
                    
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertActionStyle.default, handler: nil))
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: { (action: UIAlertAction!) in
            
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
 
    
    /*
    func facebookdata(){
        
        guard let accessToken = FBSDKAccessToken.current() else {
            print("Failed to get access token")
            return
        }
        do {
            if Auth.auth().currentUser?.uid == nil{
                FBSDKAccessToken.setCurrent(nil)
                FBSDKProfile.setCurrent(nil)
                logout()
            }else{
                let credential = try! FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                Auth.auth().signIn(with: credential) { (user, error) in
                    if let error = error {
                        print("Facebook login failed. Error \(error)")
                        return
                    }
                    
                    self.hideview.isHidden = true
                    self.profile.isHidden = true
                    
                }
            }
            
        }catch{
            print(error.localizedDescription)
            
        }
        
    }
 */
    func googledata(){
        
        if let user = Auth.auth().currentUser {
            self.hideview.isHidden = true
            self.profile.isHidden = true
    
            
            let url = URL(string: (user.photoURL?.absoluteString)!)
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async {
                   
                }
            }
        }
        
    }
    
    
    func logout(){
        GIDSignIn.sharedInstance().signOut()
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        
        try! Auth.auth().signOut()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        FBSDKAccessToken.setCurrent(nil)
        FBSDKProfile.setCurrent(nil)
        let defaults = UserDefaults.standard
        var g = String()
        if let stringOne = defaults.string(forKey: menuTableViewController.defaultsKeys.keyOne) {
            print(stringOne)
            g = stringOne
        }
        defaults.removeObject(forKey: menuTableViewController.defaultsKeys.keyOne)
        let loginViewController = storyboard.instantiateViewController(withIdentifier: "loginscreen")
        present(loginViewController, animated: true, completion: nil)
    }
    

    
}
